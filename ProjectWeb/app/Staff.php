<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
  protected $fillable =['password',
        'id_card',
            'firstname',
            'lastname',
            'sex',
            'address',
            'email',
            'phone',
        'position'];
}
