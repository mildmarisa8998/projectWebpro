<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\Flight;
class AirlineUserController extends Controller
{

	public function reserve(Request $request,$id){
		session_start();
		$seat=$request->input('value');
		$request = $request->all();
		$cid=$_SESSION['id'];
		$result = array(
			'cid' => $cid,
			'fid' => $id,
			'seat' => $seat,
			);
		DB::table('reserves')->insert($result);

		$data = DB::table('customers')->where('cid',$cid)->get();
		$datas = DB::table('customers')->where('cid',$cid)->get();
		$_SESSION['account']=$datas[0]->firstname;
		$flight = Flight::all()->toArray();
		$his=DB::select('select * from
		 reserves
		 inner join customers on reserves.cid = customers.cid
		 inner join flights on reserves.fid = flights.fid
		 where customers.cid=?',[$data[0]->cid]);
		return redirect('HomeLogin')
		->with(compact('data'))
		->with(compact('flight'))
		->with(compact('his'))->with('success',"Reserve Successfully");
	}
		public  function login(Request $request){
				session_start();
				$name=$request->input('username');
				$pass=$request->input('pass');
				$data = DB::select('select * from admins where user=? and password=?',[$name,$pass]);
			//	dd($data[0]->user);
				if(count($data)){
					$_SESSION["account"] = $data[0]->name;
					$_SESSION["id"] = $data[0]->Aid;
						return view('Board');
				}
				else{
					$data = DB::select('select * from staff where id_card=? and password=?',[$name,$pass]);
					if(count($data)){
						$_SESSION["account"] = $data[0]->firstname;
						$_SESSION["id"] = $data[0]->sid;
						return view('BoardStaff.BoardStaff');
					}
					else{
							session_destroy();
						return back()->withInput();
					}

				}
		}
		public  function loginC(Request $request){
				session_start();
				$name=$request->input('email');
				$pass=$request->input('password');

				$data = DB::select('select * from admins where user=? and password=?',[$name,$pass]);
			//	dd($data[0]->user);

				if(count($data)>0){
					$_SESSION["account"] = $data[0]->name;
					$_SESSION["id"] = $data[0]->Aid;
					return view('Board');
				}
				else{
					$data = DB::select('select * from staff where id_card=? and password=?',[$name,$pass]);
					if(count($data)>0){
						$_SESSION["account"] = $data[0]->firstname;
						$_SESSION["id"] = $data[0]->sid;
						return view('BoardStaff.BoardStaff',compact('data'));
					}
					else{
						$data = DB::select('select * from customers where email=? and password=?',[$name,$pass]);
						if(count($data)>0){
							$flight = Flight::all()->toArray();

							$his=DB::select('select * from reserves
							inner join customers on reserves.cid = customers.cid
							inner join flights on reserves.fid = flights.fid
							where reserves.cid=?',[$data[0]->cid]);
							$_SESSION["account"] = $data[0]->firstname;
							$_SESSION["id"] = $data[0]->cid;

							return redirect()->route('HomeLogin.index')
							->with(compact('data'))
							->with(compact('flight'))
							->with(compact('his'));
						}
						else{
								return back()->withInput();
						}
					}

				}
		}
		public function destroy($id)
    {

        session_start();
        if(!isset($_SESSION['account'])){
            return view('Login.index');
        }
        else {
          DB::table('reserves')->where('id',$id)->delete();
					return back()->withInput();
					$id=$_SESSION['id'];
					$data = DB::select('select * from customers where cid=?',[$id]);
					$flight = Flight::all()->toArray();
					$his=DB::select('select * from reserves
					inner join customers on reserves.cid = customers.id_card
					inner join flights on reserves.fid = flights.fid
					where reserves.cid=?',[$data[0]->cid]);
					$_SESSION["account"] = $data[0]->firstname;
					$_SESSION["id"] = $data[0]->cid;
					dd(123);
					return redirect()->route('HomeLogin')
					->with(compact('data'))
					->with(compact('flight'))
				->with(compact('his'));

        }
    }
		public function Delcustomer($id){
			DB::table('reserves')->where('id',$id)->delete();
			return redirect('Home.index');
		}

		public function update(Request $request)
		{

			$data = $request->all();

			$result = array(
				'password' => $data['password'],
				'id_card' => $data['idcard'],
				'firstname' => $data['fname'],
				'lastname' => $data['lname'],
				'sex' => $data['Sex'],
				'address' => $data['Address'],
				'email' => $data['Email'],
				'phone' => $data['phone'],
				'position' => $data['position']
			);
			 DB::table('staff')->where('sid',$data['id'])->update($result);
			 return back()->with('success','บันทึกข้อมูลเรียบร้อย');
		}
}
/*///	public function login()
//	{

		// $user = $reg->input('Username');
		// $pass = $reg->input('Password');
		//
		// $check = DB::table('admins')->where(['user'=>$user,'password'=>$pass])->get();
		// dd($check);
		// if(count($check)>0){
		// 	  return redirect()->route('Staff.index');
		// }
		// else {
		// 	// $check = DB::table('staff')->where(['id_card'=>$user,'password'=>$pass])->get();
		// 	// if(count($check)>0){
  	// 	// 	return redirect()->route('Customer.index');
		// 	// }
		// 	// else {
		// 	// 	return viwe('LoginAdmin');
		// 	// }
		// 	return viwe('LoginAdmin');
		//}
	}
    //Customer
	public  function Qcustomer(){
		$customers = DB::table('customers')->get();
		return view('backend.customer',['customer'=>$customers]);
	}
	public function InsertCustomer(Request $request){
		$data = $request->all();
		$result = array(
			'password' => $data['password'],
			'id_card' => $data['idcard'],
			'firstname' => $data['fname'],
			'lastname' => $data['lname'],
			'sex' => $data['Sex'],
			'address' => $data['Address'],
			'email' => $data['Email'],
			'phone' => $data['phone'],
			'status' => '0'
		);
		DB::table('customers')->insert($result);
		return redirect('customer');
	}
	public function Delcustomer($cid){
		DB::table('customers')->where('cid',$cid)->delete();
		return redirect('customer');
	}
	public function Upcustomer(Request $request){
		$data = $request->all();
		$result = array(
			'password' => $data['password'],
			'id_card' => $data['idcard'],
			'firstname' => $data['fname'],
			'lastname' => $data['lname'],
			'sex' => $data['Sex'],
			'address' => $data['Address'],
			'email' => $data['Email'],
			'phone' => $data['phone'],
			'status' => '0'
		);
		DB::table('customers')->where('cid',$data['cid'])->update($result);
		return redirect('customer');
	}
	public function Editecustomer($cid){
		$customers = DB::table('customers')->where('cid',$cid)->get();
		return view('backend.Editecustomer',['customer'=>$customers]);
	}

	//STAFF
	public  function Qstaff(){
		$staffs = DB::table('staff')->get();
		return view('backend.staff',['staff'=>$staffs]);
	}
	public function InsertStaff(Request $request){
		$data = $request->all();
		$result = array(
			'password' => $data['password'],
			'id_card' => $data['idcard'],
			'firstname' => $data['fname'],
			'lastname' => $data['lname'],
			'sex' => $data['Sex'],
			'address' => $data['Address'],
			'email' => $data['Email'],
			'phone' => $data['phone'],
			'position' => $data['position'],
			'status' => '0'
		);
		DB::table('staff')->insert($result);
		return redirect('staff');
	}
	public function Delstaff($cid){
		DB::table('staff')->where('sid',$sid)->delete();
		return redirect('staff');
	}
	public function Upstaff(Request $request){
		$data = $request->all();
		$result = array(
			'password' => $data['password'],
			'id_card' => $data['idcard'],
			'firstname' => $data['fname'],
			'lastname' => $data['lname'],
			'sex' => $data['Sex'],
			'address' => $data['Address'],
			'email' => $data['Email'],
			'phone' => $data['phone'],
			'position'=> $data['position'],
			'status' => '0'
		);
		DB::table('staff')->where('sid',$data['sid'])->update($result);
		return redirect('staff');
	}
	public function Editestaff($sid){
		$staffs = DB::table('staff')->where('sid',$sid)->get();
		return view('backend.Editestaff',['staff'=>$staffs]);
	}

	//Flight
	//STAFF
	public  function QFlight(){
		$staffs = DB::table('flight')->get();
		return view('backend.flight',['flight'=>$flights]);
	}
	public function InsertFlight(Request $request){
		$data = $request->all();
		$result = array(
			'planename' => $data['Plane'],
			'source' => $data['Source'],
			'destinetion' => $data['Destination'],
			'startDate' => $data['Departure'],
			'endDate' => $data['Arrival'],
			'price' => $data['Price']
		);
		DB::table('flight')->insert($result);
		return redirect('flight');
	}
	public function DelFlight($cid){
		DB::table('flight')->where('fid',$fid)->delete();
		return redirect('flight');
	}
	public function UpFlight(Request $request){
		$data = $request->all();
		$result = array(
			'planename' => $data['Plane'],
			'source' => $data['Source'],
			'destinetion' => $data['Destination'],
			'startDate' => $data['Departure'],
			'endDate' => $data['Arrival'],

			'price' => $data['Price']
		);
		DB::table('flight')->where('fid',$data['fid'])->update($result);
		return redirect('flight');
	}
	public function EditeFlight($cid){
		$staffs = DB::table('flight')->where('fid',$fid)->get();
		return view('backend.EditeFlight',['flight'=>$flights]);
	}
}
*/
