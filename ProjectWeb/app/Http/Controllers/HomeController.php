<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\customer;
use DB;
use App\Flight;
class HomeController extends Controller
{

    public function index()
    {
      session_start();
      session_destroy();
      $flight = Flight::all()->toArray();
      return view('Home.index',compact('flight'));

   }

   /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function create()
   {
     session_start();
     	if(!isset($_SESSION['account'])){
     			return redirect()->route('Home.index')->with('success','Please login');
     	}
      else{
        $name=$_SESSION['id'];
        $data = DB::select('select * from customers where cid=?',[$name]);
        $flight = Flight::all()->toArray();
        $his=DB::select('select * from
         reserves
         inner join customers on reserves.cid = customers.cid
         inner join flights on reserves.fid = flights.fid
         where customers.cid=?',[$data[0]->cid]);

        return view('Home.create')->with(compact('flight'))
        ->with(compact('his'))
        ->with(compact('data'));
      }
   }

   /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function store(Request $request)
   {

     $this->validate($request,
     ['password'=>'required',
         'idcard'=>'required',
         'firstname'=>'required',
         'lastname'=>'required',
         'Sex'=>'required',
         'adress'=>'required',
         'email'=>'required',
         'phone'=>'required']);

      $customers =new customer([
            'password'=>$request->get('password'),
            'id_card'=>$request->get('idcard'),
            'firstname'=>$request->get('firstname'),
            'lastname'=>$request->get('lastname'),
            'sex'=>$request->get ('Sex'),
            'address'=>$request->get ('adress'),
            'email'=>$request->get('email'),
            'phone'=>$request->get('phone')]);
        $customers->save();
      return redirect()->route('Home.index')->with('success','บันทึกข้อมูลเรียบร้อย');

      }

   /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function show($id)
   {
       //
   }

   /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function edit($id)
   {
        session_start();
        if(!isset($_SESSION['account'])){
            return redirect()->route('Home.index')->with('success','Please login');
        }
       else{
          $data = DB::table('customers')->where('cid',$id)->get();
          return view('Home.edit',compact('data'));
        }
   }

   /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function update(Request $request)
   {
     session_start();
     if(!isset($_SESSION['account'])){
         return redirect()->route('Home.index')->with('success','Please login');
     }
    else{
      $datas = $request->all();
      $result = array(
            'password' => $datas['password'],
            'id_card' => $datas['idcard'],
            'firstname' => $datas['firstname'],
            'lastname' => $datas['lastname'],
           'sex' => $datas['sex'],
            'address' => $datas['address'],
            'email' => $datas['email'],
            'phone' => $datas['phone']
          );
          DB::table('customers')->where('cid',$datas['id'])->update($result);

          $data = DB::table('customers')->where('cid',$datas['id'])->get();
          $_SESSION['account']=$datas['firstname'];
          $flight = Flight::all()->toArray();
          $his=DB::select('select * from
           reserves
           inner join customers on reserves.cid = customers.id_card
           inner join flights on reserves.fid = flights.fid
           where customers.cid=?',[$data[0]->cid]);
          return view('Home.create')
          ->with(compact('data'))
          ->with(compact('flight'))
          ->with(compact('his'));
        }

   }

   /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function destroy($id)
   {
       dd($id);
       session_start();
       if(!isset($_SESSION['account'])){
           return view('Login.index');
       }
       else{
         DB::table('reserves')->where('id',$id)->delete();
         return redirect()->route('Reserve.index')->with('success','ลบข้อมูลเรียบนร้อย');
       }
   }
}
