<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\Flight;
class FlightController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      session_start();
      if(!isset($_SESSION['account'])){
          return view('Login.index');
      }
      else{
        $flight = Flight::all()->toArray();
        return view('Flight.index',compact('flight'));
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Flight.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request,
      ['Plane'=>'required',
          'Source'=>'required',
          'Destination'=>'required',
          'Depart'=>'required',
          'Return'=>'required',
          'Price'=>'required',
        ]);


        $Flight =new Flight(
            ['planename'=>$request->get('Plane'),
            'source'=>$request->get('Source'),
            'destinetion'=>$request->get('Destination'),
            //  'startDate'=>$request->get('Depart'),
              'startDate'=>date('Y-m-d', strtotime(str_replace('-', '/', $request['Depart']))),
              //'endDate'=>$request->get ('Return'),
              'endDate'=>date('Y-m-d', strtotime(str_replace('-', '/', $request['Return']))),
            'price'=>$request->get ('Price')
            ]);

        $Flight->save();
        return redirect()->route('Flight.index')->with('success','บันทึกข้อมูล');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      session_start();
      if(!isset($_SESSION['account'])){
          return view('Login.index');
      }
      else{
        $Flight = DB::table('flights')->where('fid',$id)->get();
        return view('Flight.edit',['flight'=>$Flight]);
      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $this->validate($request,
      ['Plane'=>'required',
          'Source'=>'required',
          'Destination'=>'required',
          'Depart'=>'required',
          'Return'=>'required',
          'Price'=>'required',
        ]);

      //
      //   $Flight =new Flight(
      //       ['planename'=>$request->get('Plane'),
      //       'source'=>$request->get('Source'),
      //       'destinetion'=>$request->get('Destination'),
      //     //  'startDate'=>$request->get('Depart'),
      //       'startDate'=>date('Y-m-d', strtotime(str_replace('-', '/', $request['Depart']))),
      //       //'endDate'=>$request->get ('Return'),
      //       'endDate'=>date('Y-m-d', strtotime(str_replace('-', '/', $request['Return']))),
      //
      //       'price'=>$request->integer('Price')
      //       ]);
      // $customers->save();
      $data = $request->all();
      $result = array(
        'planename' => $data['Plane'],
        'source' => $data['Source'],
        'destinetion' => $data['Destination'],
        'startDate' =>date('Y-m-d', strtotime(str_replace('-', '/', $data['Depart']))),
        'endDate' => date('Y-m-d', strtotime(str_replace('-', '/', $data['Return']))),
        'price' => $data['Price']
      );
      DB::table('flights')->where('fid',$data['id'])->update($result);
      return redirect()->route('Flight.index')->with('success','อัพเดทข้อมูลเรียบร้อย');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      session_start();
      if(!isset($_SESSION['account'])){
          return view('Login.index');
      }
      else{
        DB::table('flights')->where('fid',$id)->delete();
        return redirect()->route('Flight.index')->with('success','ลบข้อมูลเรียบนร้อย');
      }
    }
}
