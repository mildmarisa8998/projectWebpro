<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\Staff;
class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      session_start();
      if(!isset($_SESSION['account'])){
          return view('Login.index');
      }
      else{
        $staff = Staff::all()->toArray();
        return view('Staff.index',compact('staff'));
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      session_start();
      if(!isset($_SESSION['account'])){
          return view('Login.index');
      }
      else{
        return view('Staff.create');
      }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request,
      ['password'=>'required',
          'idcard'=>'required',
          'fname'=>'required',
          'lname'=>'required',
          'Sex'=>'required',
          'Address'=>'required',
          'Email'=>'required',
          'phone'=>'required',
        'position'=>'required']);
        $staff =new Staff(
            ['password'=>$request->get('password'),
            'id_card'=>$request->get('idcard'),
            'firstname'=>$request->get('fname'),
            'lastname'=>$request->get('lname'),
            'sex'=>$request->get ('Sex'),
            'address'=>$request->get ('Address'),
            'email'=>$request->get('Email'),
            'phone'=>$request->get('phone'),
          'position' =>$request->get('position')]);

        $staff->save();
        return redirect()->route('Staff.index')->with('success','บันทึกข้อมูล');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      session_start();
      if(!isset($_SESSION['account'])){
          return view('Login.index');
      }
      else{
        $staff = DB::table('staff')->where('sid',$id)->get();
        return view('Staff.edit',['staff'=>$staff]);
      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      // $this->validate($request,['password'=>'required',
      //     'idcard'=>'required',
      //     'fname'=>'required',
      //     'lname'=>'required',
      //     'Sex'=>'required',
      //     'Address'=>'required',
      //     'Email'=>'required',
      //     'phone'=>'required',
      //   'position'=>'required']);
      $data = $request->all();

      $result = array(
        'password' => $data['password'],
        'id_card' => $data['idcard'],
        'firstname' => $data['fname'],
        'lastname' => $data['lname'],
        'sex' => $data['Sex'],
        'address' => $data['Address'],
        'email' => $data['Email'],
        'phone' => $data['phone'],
        'position' => $data['position']
      );
       DB::table('staff')->where('sid',$data['id'])->update($result);
       return redirect()->route('Staff.index')->with('success','บันทึกข้อมูล');
      
            // $customers =new Staff(
            //     ['password'=>$request->get('password'),
            //     'id_card'=>$request->get('idcard'),
            //     'firstname'=>$request->get('fname'),
            //     'lastname'=>$request->get('lname'),
            //     'sex'=>$request->get ('Sex'),
            //     'address'=>$request->get ('Address'),
            //     'email'=>$request->get('Email'),
            //     'phone'=>$request->get('phone')]);
            // $customers->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      session_start();
      if(!isset($_SESSION['account'])){
          return view('Login.index');
      }
      else{
        DB::table('staff')->where('sid',$id)->delete();
        return redirect()->route('Staff.index')->with('success','ลบข้อมูลเรียบนร้อย');
      }
    }
}
