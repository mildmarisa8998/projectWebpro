<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class ReserveForCustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      session_start();
      if(!isset($_SESSION['account'])){
          return view('Login.index');
      }
      else{
        $data=DB::select('select * from
        reserves
        inner join customers on reserves.cid = customers.cid
        inner join flights on reserves.fid = flights.fid');
        return view('BoardStaff.ReserveForCustomer.index',compact('data'));
      }
    }
    public function reservestaff(Request $request){

  		$seat=$request->input('value');
      $fid=$request->input('fid');
      $cid= $request->input('cid');
  		$request = $request->all();

  		$result = array(
  			'cid' => $cid,
  			'fid' => $fid,
  			'seat' => $seat,
  			);
  		DB::table('reserves')->insert($result);

  		return redirect('ReserveForCustomerIndex');
  	}
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
