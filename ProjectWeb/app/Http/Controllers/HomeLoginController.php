<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Flight;
class HomeLoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        session_start();
        if(!isset($_SESSION['account'])){
            return redirect()->route('Home.index')->with('success','Please login');
        }
        else{
          $name=$_SESSION['id'];
          $data = DB::select('select * from customers where cid=?',[$name]);
          $flight = Flight::all()->toArray();
          $his=DB::select('select * from
           reserves
           inner join customers on reserves.cid = customers.cid
           inner join flights on reserves.fid = flights.fid
           where customers.cid=?',[$data[0]->cid]);

          return view('Home.create')->with(compact('flight'))
          ->with(compact('his'))
          ->with(compact('data'));
          return view('Home.create');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      session_start();
      if(!isset($_SESSION['account'])){
          return view('Login.index');
      }
      else {
        DB::table('reserves')->where('id',$id)->delete();
        return back()->withInput();
        $id=$_SESSION['id'];
        $data = DB::select('select * from customers where cid=?',[$id]);
        $flight = Flight::all()->toArray();

        $his=DB::select('select * from reserves
        inner join customers on reserves.cid = customers.cid
        inner join flights on reserves.fid = flights.fid
        where reserves.cid=?',[$data[0]->cid]);

        $_SESSION["account"] = $data[0]->firstname;
        $_SESSION["id"] = $data[0]->cid;

        return redirect()->route('HomeLogin')
        ->with(compact('data'))
        ->with(compact('flight'))
      ->with(compact('his'))->with('success',"Cancel Successfully");
    }
    }
}
