<?php

namespace App\Http\Controllers;

use App\InboardModel;
use Illuminate\Http\Request;
use DB;
use App\Flight;

class InboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function createReserve(Request $request)
    {

        $id = $request['name1'];
        $allData = DB::table('customers')->where('id_card',$id) -> get();

        return view("BoardStaff.ReserveForCustomer.showDatacustomer",['allData' => $allData]);
    }

     public function dataReserve(Request $request)
    {
        $cid = $request->input('id');
        $source = $request->input('source');
        $destinetion = $request->input('destinetion');
        $Depart = $request->input('startDate');
        $Return = $request->input('endDate');

        $allData = DB::select('select * from flights where source=? or destinetion=?',[$source,$destinetion]);
        $flight = Flight::all()->toArray();
        $customer =DB::table('customers')->where('cid',$cid)->get();
        return view('BoardStaff.ReserveForCustomer.showTableFlight')
    		->with(compact('allData'))
    		->with(compact('flight'))->with(compact('customer'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InboardModel  $inboardModel
     * @return \Illuminate\Http\Response
     */
    public function show(InboardModel $inboardModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InboardModel  $inboardModel
     * @return \Illuminate\Http\Response
     */
    public function edit(InboardModel $inboardModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InboardModel  $inboardModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InboardModel $inboardModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InboardModel  $inboardModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(InboardModel $inboardModel)
    {
        //
    }

}
