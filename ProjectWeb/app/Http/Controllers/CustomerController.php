<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\customer;
class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      session_start();
      if(!isset($_SESSION['account'])){
        	return view('Login.index');
      }
     else{
        $customers = customer::all()->toArray();
        return view('Customer.index',compact('customers'));
      }
  }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      session_start();
      if(!isset($_SESSION['account'])){
          return view('Login.index');
      }
      else{
          return view('Customer.create');
      }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
        ['password'=>'required',
            'idcard'=>'required',
            'fname'=>'required',
            'lname'=>'required',
            'Sex'=>'required',
            'Address'=>'required',
            'Email'=>'required',
            'phone'=>'required']);

        $customers =new customer(
            ['password'=>$request->get('password'),
            'id_card'=>$request->get('idcard'),
            'firstname'=>$request->get('fname'),
            'lastname'=>$request->get('lname'),
            'sex'=>$request->get ('Sex'),
            'address'=>$request->get ('Address'),
            'email'=>$request->get('Email'),
            'phone'=>$request->get('phone')]);
        $customers->save();
        return redirect()->route('Customer.index')->with('success','บันทึกข้อมูล');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      session_start();
      if(!isset($_SESSION['account'])){
          return view('Login.index');
      }
      else{
        $customer = DB::table('customers')->where('cid',$id)->get();
        return view('Customer.edit',['customer'=>$customer]);
      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $this->validate($request,['password'=>'required',
          'idcard'=>'required',
          'fname'=>'required',
          'lname'=>'required',
          'Sex'=>'required',
          'Address'=>'required',
          'Email'=>'required',
          'phone'=>'required']);
          $data = $request->all();
       		$result = array(
       			'password' => $data['password'],
       			'id_card' => $data['idcard'],
       			'firstname' => $data['fname'],
       			'lastname' => $data['lname'],
            'sex' => $data['Sex'],
       			'address' => $data['Address'],
       			'email' => $data['Email'],
       			'phone' => $data['phone']
       		);
       		DB::table('customers')->where('cid',$data['id'])->update($result);
          return redirect()->route('Customer.index')->with('success','อัพเดทข้อมูลเรียบร้อย');
      // $customers = customer($id);
      // $customers->password=$request->get('password');
      // $customers->id_card=$request->get('idcard');
      // $customers->firstname=$request->get('fname');
      // $customers->lastname=$request->get('lname');
      // $customers->sex=$request->get ('Sex');
      // $customers->address=$request->get ('Address');
      // $customers->email=$request->get('Email');
      // $customers->phone=$request->get('phone');
      // $customers->save();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      session_start();
      if(!isset($_SESSION['account'])){
          return view('Login.index');
      }
      else{
        DB::table('customers')->where('cid',$id)->delete();
        return redirect()->route('Customer.index')->with('success','ลบข้อมูลเรียบนร้อย');
      }
    }
}
