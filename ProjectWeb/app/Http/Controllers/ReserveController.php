<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\customer;
class ReserveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //$customers = customer::all()->toArray();
        session_start();
        if(!isset($_SESSION['account'])){
            return view('Login.index');
        }
        else{
          $data=DB::select('select * from
          reserves
          join customers on reserves.cid = customers.cid
          join flights on reserves.fid = flights.fid');
          return view('Reserve.index',compact('data'));
        //dd($data);
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      dd($id);
      session_start();
      if(!isset($_SESSION['account'])){
          return view('Login.index');
      }
      else{
        DB::table('reserves')->where('id',$id)->delete();
        return redirect()->route('Reserve.index')->with('success','ลบข้อมูลเรียบนร้อย');
      }
    }
}
