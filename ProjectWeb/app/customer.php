<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class customer extends Model
{
	protected $fillable =['password',
  			'id_card',
            'firstname',
            'lastname',
            'sex',
            'address',
            'email',
            'phone',
        	'status'];
}
