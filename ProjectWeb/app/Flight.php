<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flight extends Model
{
    //
    protected $fillable =['planename',
              'source',
              'destinetion',
              'startDate',
              'endDate',
              'price'];

}
