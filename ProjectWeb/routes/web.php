<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

*/
Route::resource('HomeLogin', 'HomeLoginController');
Route::resource('Customer', 'CustomerController');
Route::resource('Staff', 'StaffController');
Route::resource('Flight', 'FlightController');
Route::resource('Reserve', 'ReserveController');
Route::resource('Home', 'HomeController');
Route::resource('EditMyself', 'EditStaffController');
Route::resource('ChackFlightForCustomer', 'StaffCheckFilghtController');
Route::resource('ReserveForCustomerIndex', 'ReserveForCustomerController');
Route::get('/logout', 'AirlineUserControlle@logout');
Route::get('/logoutbackend', 'AirlineUserControlle@logoutbackend');
Route::post('', 'AirlineUserController@login');
Route::get('Login', function () {
  session_start();
  session_destroy();
      return view('Login.index');
});
Route::get('/HomeAdmin', function () {
    return view('Board');
});
Route::post('logincheck', 'AirlineUserController@login');
Route::post('logincheckCustomer', 'AirlineUserController@loginC');
Route::post('Edit', 'AirlineUserController@login');
Route::post('/CustomerUpdate', 'CustomerController@update');
Route::post('/StaffUpdate', 'StaffController@update');
Route::post('/StaffsUpdates', 'AirlineUserController@update');
Route::post('/FlightUpdate', 'FlightController@update');
Route::post('/CustomerHomeUpdate', 'HomeController@update');
Route::post('/addminsUpdate', 'AdminsController@update');
Route::get('/seatAirline', function () {
    return view('seatAirline');
});
Route::get('/customer', function () {
    return view('customer');
});
Route::get('/confrimSeat', function () {
    return view('confrirmSeat');
});
Route::get('/edit', function () {
    return view('Home.index');
});

//Route::get('/delete_data/{id}', 'HomeLoginController@destroy');
Route::post('/reserve/{id}', 'AirlineUserController@reserve');
///////////////////////////////////////////////////////////////////
Route::get('/BoardStaff', function () {
    return view('BoardStaff.BoardStaff');
});
Route::get('/masterStaff', function () {
    return view('BoardStaff.masterStaff');
});
// Route::get('/EditMyself', function () {
//     return view('BoardStaff.EditMyself.editStaffMyself');
// });

// Route::get('/ChackFlightForCustomer', function () {
//     return view('BoardStaff.ChackFlightForCustomer.ShowFlight');
// });

Route::get('/CreateFlightByStaff', function () {
    return view('BoardStaff.ChackFlightForCustomer.createFlight');
});

// Route::get('/ReserveForCustomerIndex', function () {
//     return view('BoardStaff.ReserveForCustomer.indexReserve');
// });

Route::get('/ReserveForCustomerCreate', function () {
    return view('BoardStaff.ReserveForCustomer.createReserve');
});

Route::get('/ReserveForCustomerEdit', function () {
    return view('BoardStaff.ReserveForCustomer.editReserve');
});

//////mild
Route::post('/showCustomerByStaff', 'InboardController@createReserve');

Route::post('/showTableFligthByStaff','InboardController@dataReserve');

Route::post('/ReserveForCustomerIndex','ReserveForCustomerController@reservestaff');
Route::resource('EditeMyselfAdmin', 'AdminsController');
/*Route::post('/InsertCustomer', 'AirlineUserController@InsertCustomer');
Route::get('/customer', 'AirlineUserController@Qcustomer');
Route::get('/customerdel/{cid}', 'AirlineUserController@Delcustomer');
Route::get('/Editecustomer/{cid}', 'AirlineUserController@Editecustomer');
Route::post('/Upcustomer', 'AirlineUserController@Upcustomer');


Route::post('/InsertStaff', 'AirlineUserController@InsertStaff');
Route::get('/staff', 'AirlineUserController@Qstaff');
Route::get('/staffdel/{cid}', 'AirlineUserController@Delstaff');
Route::get('/Editestaff/{cid}', 'AirlineUserController@Editestaff');
Route::post('/Upstaff', 'AirlineUserController@Upstaff');


Route::post('/InsertFlight', 'AirlineUserController@InsertFlight');
Route::get('/Flight', 'AirlineUserController@QFlight');
Route::get('/Flight/{fid}', 'AirlineUserController@DelFlight');
Route::get('/EditeFlight/{cid}', 'AirlineUserController@EditeFlight');
Route::post('/UpFlight', 'AirlineUserController@UpFlight');



Route::get('/index', function () {
    return view('home');
});
Route::get('/login', function () {
    return view('backend.LoginAdmin');
});
Route::get('/register', function () {
    return view('backend.Register');
});
Route::get('/Admin', function () {
    return view('backend.Board');
});
Route::get('/flight', function () {
    return view('backend.flight');
});
Route::get('/reserve', function () {
    return view('backend.reserve');
});
Route::get('/addcustomer', function () {
    return view('backend.addcustomer');
});

Route::get('/addstaff', function () {
    return view('backend.addstaff');
});

Route::get('/addflight', function () {
    return view('backend.addflight');
});
*/
?>
