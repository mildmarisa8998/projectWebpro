@extends('BoardStaff.masterStaff')
@section('title','Add Flight')
@section('head')
@stop

@section('content')
<div class="container mt-5 mb-5">
	<h1 class="display-4 mt-5 mb-5">Add Flight ByStaff</h1>
	<div class="row">
		
		<div class="col-lg-4">
			<form action="{{url('Flight')}}" method="post" class="pt-5 pb-5">
				{{ csrf_field() }}
				<div class="form-group">
					<label for="Plane">Plane</label>
					<input type="text" class="form-control" id="Plane" placeholder="Plane" name="Plane">
				</div>
				<div class="form-group">
					<label for="Source">Source</label>
					<input type="text" class="form-control" id="Source" placeholder="Source" name="Source">
				</div>
				<div class="form-group">
					<label for="Destination">Destination</label>
					<input type="text" class="form-control" id="Destination" placeholder="Destination" name="Destination">
				</div>
				<div class="form-group">
					<label for="Depart">Depart</label>
					<input class="date form-control" type="text" id="Depart" placeholder="Depart" name="Depart">
				</div>
				<div class="form-group">
					<label for="Return">Return</label>
					<input class="date form-control" type="Return" id="Return" placeholder="Return" name="Return">
				</div>
				<script type="text/javascript">
					$('.date').datepicker({
						format: 'mm-dd-yyyy'
					});
				</script>
				<div class="form-group">
					<label for="Price">Price</label>
					<input type="text" class="form-control" id="Price" placeholder="Price" name="Price">
				</div>
				<button type="submit" class="btn btn-primary">Add Flight</button>
			</form>
		</div>
	</div>
</div>
@stop
@section('footer')
@stop
