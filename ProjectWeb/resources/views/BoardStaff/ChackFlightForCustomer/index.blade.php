@extends('BoardStaff.masterStaff')
@section('title','Flight')
@section('head')
@stop
@section('content')

<style>
	*{
        box-sizing: border-box;
      }

      #myInput,#myInput1,#myInput2,#myInput3 {
        background-image: url('/css/searchicon.png');
        background-position: 10px 10px;
        background-repeat: no-repeat;
        width: 15%;
        font-size: 16px;
        padding: 12px 20px 12px 40px;
        border: 1px solid #ddd;
        margin-bottom: 12px;
        margin-left: 10px;
      }

      #myTable {
        border-collapse: collapse;
        width: 100%;
        border: 1px solid #ddd;
        font-size: 18px;
        text-align: center;
      }

      #myTable th, #myTable td {
        text-align: left;
        padding: 12px;
        text-align: center;
      }

      #myTable tr {
        border-bottom: 1px solid #ddd;
      }

      #myTable tr.header, #myTable tr:hover {
        background-color: #f1f1f1;
      }
</style>

<div id="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="#">Dashboard</a>
			</li>
			<li class="breadcrumb-item active">Flight</li>
		</ol>
		<!-- Icon Cards-->
		<div id="content-wrapper">
			<div class="container mb-5">
				<!-- <a href="{{url('CreateFlightByStaff')}}">
					<button type="submit" class="btn btn-primary">Add Flight</button>
				</a> -->

				<h1 class="display-4 mt-5 mb-5">Flight</h1>
				<input type="text" id="myInput" onkeyup="myFunction()"
             		placeholder="ต้นทาง" title="Type in a name">
             	<input type="text" id="myInput1" onkeyup="myFunction1()"
             		placeholder="ปลายทาง" title="Type in a name">
				<input type="text" id="myInput2" onkeyup="myFunction2()"
             		placeholder="วันออกเดินทาง" title="Type in a name">
				<input type="text" id="myInput3" onkeyup="myFunction3()"
             		placeholder="วันกลับ" title="Type in a name">


				<div class="row">
					<div class="form-row w-100">
						<div class="table-responsive">
							<table class="table table-hover" id = myTable>
								<tr class="header">
									<th>Plane</th>
									<th>Source</th>
									<th>Destinetion</th>
									<th>Depart</th>
									<th>Return</th>
									<th>Price</th>
									<th></th>
									<th></th>
								</tr>
								@foreach($flight as $row)
								<tr>
									<td>{{$row['planename']}}</td>
									<td>{{$row['source']}}</td>
									<td>{{$row['destinetion']}}</td>
									<td>{{$row['startDate']}}</td>
									<td>{{$row['endDate']}}</td>
									<td>{{$row['price']}}</td>
								</tr>
								@endforeach
							</table>
						</tbody>
					</div>
				</div>
			</div>
		</div>
				<script type="text/javascript" src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
				<script type="text/javascript" src="{{ asset('bootstrap/js/bootstrap.bundle.js') }}"></script>

				<!-- Sticky Footer -->
				<footer class="sticky-footer">
					<div class="container my-auto">
						<div class="copyright text-center my-auto">
							<span>Board Staff Welcom to Airline</span>
						</div>
					</div>
				</footer>

			</div>
			<!-- /.container-fluid -->

			<!-- Sticky Footer -->
			<footer class="sticky-footer">
				<div class="container my-auto">
					<div class="copyright text-center my-auto">
						<span>Welcom to Airline</span>
					</div>
				</div>
			</footer>
		</div>


		<script>
			function myFunction() {
				  var input, filter, table, tr, td, i, txtValue;
				  input = document.getElementById("myInput");
				  filter = input.value.toUpperCase();
				  table = document.getElementById("myTable");
				  tr = table.getElementsByTagName("tr");
				        for (i = 0; i < tr.length; i++) {
				          td = tr[i].getElementsByTagName("td")[1];
				          if (td) {
				            txtValue = td.textContent || td.innerText;
				            if (txtValue.toUpperCase().indexOf(filter) > -1) {
				              tr[i].style.display = "";
				            } else {
				              tr[i].style.display = "none";
				            }
				          }
				        }
			}
		</script>

<script>
		function myFunction1() {
		  var input, filter, table, tr, td, i, txtValue;
		  input = document.getElementById("myInput1");
		  filter = input.value.toUpperCase();
		  table = document.getElementById("myTable");
		  tr = table.getElementsByTagName("tr");
		        for (i = 0; i < tr.length; i++) {
		          td = tr[i].getElementsByTagName("td")[2];
		          if (td) {
		            txtValue = td.textContent || td.innerText;
		            if (txtValue.toUpperCase().indexOf(filter) > -1) {
		              tr[i].style.display = "";
		            } else {
		              tr[i].style.display = "none";
		            }
		          }
		        }
		}
</script>

<script>
		function myFunction2() {
		  var input, filter, table, tr, td, i, txtValue;
		  input = document.getElementById("myInput2");
		  filter = input.value.toUpperCase();
		  table = document.getElementById("myTable");
		  tr = table.getElementsByTagName("tr");
		        for (i = 0; i < tr.length; i++) {
		          td = tr[i].getElementsByTagName("td")[3];
		          if (td) {
		            txtValue = td.textContent || td.innerText;
		            if (txtValue.toUpperCase().indexOf(filter) > -1) {
		              tr[i].style.display = "";
		            } else {
		              tr[i].style.display = "none";
		            }
		          }
		        }
		}
</script>

<script>
		function myFunction3() {
		  var input, filter, table, tr, td, i, txtValue;
		  input = document.getElementById("myInput3");
		  filter = input.value.toUpperCase();
		  table = document.getElementById("myTable");
		  tr = table.getElementsByTagName("tr");
		        for (i = 0; i < tr.length; i++) {
		          td = tr[i].getElementsByTagName("td")[4];
		          if (td) {
		            txtValue = td.textContent || td.innerText;
		            if (txtValue.toUpperCase().indexOf(filter) > -1) {
		              tr[i].style.display = "";
		            } else {
		              tr[i].style.display = "none";
		            }
		          }
		        }
		}
		</script>

		@stop
		@section('footer')
		@stop
