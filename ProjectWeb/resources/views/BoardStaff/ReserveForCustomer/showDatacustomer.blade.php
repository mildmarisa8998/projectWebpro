@extends('BoardStaff.masterStaff')
@section('title','Reserve')
@section('head')
@stop
@section('content')
<style>
	*{
        box-sizing: border-box;
      }

      #myInput,#myInput1,#myInput2,#myInput3 {
        background-image: url('/css/searchicon.png');
        background-position: 10px 10px;
        background-repeat: no-repeat;
        width: 15%;
        font-size: 16px;
        padding: 12px 20px 12px 40px;
        border: 1px solid #ddd;
        margin-bottom: 12px;
        margin-left: 10px;
      }

      #myTable {
        border-collapse: collapse;
        width: 100%;
        border: 1px solid #ddd;
        font-size: 18px;
        text-align: center;
      }

      #myTable th, #myTable td {
        text-align: left;
        padding: 12px;
        text-align: center;
      }

      #myTable tr {
        border-bottom: 1px solid #ddd;
      }

      #myTable tr.header, #myTable tr:hover {
        background-color: #f1f1f1;
      }
</style>

<div class="container mt-5 mb-5">
	<h1 class="display-4 mt-5 mb-5">Reserve For Customer</h1>
	<div class="row">
		<div class="col-lg-4">
			<form action="/showTableFligthByStaff" method="post" class="pt-5 pb-5">
				{{ csrf_field() }}
				<label for="Depart">ID</label>
				<input type="text" class="form-control" style= "margin-top: 10px; width: 500px; " id="id" value="<?=$allData[0]->cid?>" name="id" >
				<label for="Depart">FristName</label>
				<input type="text" class="form-control" style= "margin-top: 10px; width: 500px; " id="fristname" value="<?=$allData[0]->firstname?>" name="fristname" >
				<label for="Depart">LastName</label>
				<input type="text" class="form-control" style= "margin-top: 10px; margin-bottom: 20px; width: 500px; " id="lastname" value="<?=$allData[0]->lastname?>" name="lastname">
				<select name = "source" class="form-control" style="width: 500px; margin-top: 10px;">
					 <?php
						  $flights = DB::table('flights')->get();
						  foreach ($flights as $flights) {
									  ?>
										<option value="<?=$flights->source?>"><?=$flights->source?></option>
					  	<?php
					  	}
					  ?>
				</select>

				<select names = "destinetion" class="form-control" style="width: 500px; margin-top: 10px; margin-bottom: 20px;">
					 <?php
						  $flights = DB::table('flights')->get();

						  foreach ($flights as $flights) {
					  ?>
								<option value="<?=$flights->destinetion?>"><?=$flights->destinetion?></option>
							  <?php
					  	}
					  ?>
				</select>

				<div class="form-group">
					<label for="Depart">Depart</label>
					<input class="date form-control" type="text" id="Depart" placeholder="Depart" name="Depart">
				</div>

				<div class="form-group">
					<label for="Return">Return</label>
					<input class="date form-control" type="Return" id="Return" placeholder="Return" name="Return">
				</div>
				<a href="{{url('showTableFligthByStaff')}}">
					<input type="submit" value="Search" class="btn btn-primary" style= "margin-top: 10px; margin-bottom: 20px;" >
				</a>
			</form>
				<script type="text/javascript">
					$('.date').datepicker({
						format: 'mm-dd-yyyy'
					});
				</script>


		</div>
	</div>
</div>

@stop
@section('footer')
@stop
