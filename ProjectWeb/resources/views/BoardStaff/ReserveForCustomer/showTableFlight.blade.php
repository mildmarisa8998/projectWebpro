@extends('BoardStaff.masterStaff')
@section('title','Reserve')
@section('head')
@stop
@section('content')
<style>
	   {
        box-sizing: border-box;
      }

      #myInput,#myInput1,#myInput2,#myInput3 {
        background-image: url('/css/searchicon.png');
        background-position: 10px 10px;
        background-repeat: no-repeat;
        width: 15%;
        font-size: 16px;
        padding: 12px 20px 12px 40px;
        border: 1px solid #ddd;
        margin-bottom: 12px;
        margin-left: 10px;
      }

      #myTable {
        border-collapse: collapse;
        width: 100%;
        border: 1px solid #ddd;
        font-size: 18px;
        text-align: center;
      }

      #myTable th, #myTable td {
        text-align: left;
        padding: 12px;
        text-align: center;
      }

      #myTable tr {
        border-bottom: 1px solid #ddd;
      }

      #myTable tr.header, #myTable tr:hover {
        background-color: #f1f1f1;
      }
</style>

<div class="container mt-5 mb-5">
	<h1 class="display-4 mt-5 mb-5">Reserve For Customer</h1>

  <input type="text" id="myInput" onkeyup="myFunction()"
  	value="<?=$allData[0]->source?>" title="Type in a name">

  <input type="text" id="myInput1" onkeyup="myFunction1()"
      value="<?=$allData[0]->destinetion?>" title="Type in a name">

  <input type="text" id="myInput2" onkeyup="myFunction2()"
      value="<?=$allData[0]->startDate?>" title="Type in a name">

  <input type="text" id="myInput3" onkeyup="myFunction3()"
      value="<?=$allData[0]->endDate?>" title="Type in a name">


	<div class="row">
		<div class="container mt-5 mb-5">

				 <table id="myTable">
          <thead>
			        <tr class="header">
			          <th>Source</th>
			          <th>Destinetion</th>
			          <th>Depart</th>
			          <th>Return</th>
			          <th>Price</th>
								<th>Class</th>
			          <th>Plane</th>
			          <th></th>
			        </tr>
			     </thead>
					 <form method="post" class="delete_form" action="/ReserveForCustomerIndex">
						{{ csrf_field() }}
						<input type="hidden" id="cid" name="cid"
								value="<?=$customer[0]->cid?>" title="Type in a name">

					 @foreach($flight as $row)
						<tr>

						 <td>{{$row['source']}}</td>
						 <td>{{$row['destinetion']}}</td>
						 <td>{{$row['startDate']}}</td>
						 <td>{{$row['endDate']}}</td>
						 <td>{{$row['price']}}</td>
						 <td>
							 <select name="value">
									<option value="First class">First class</option>
									<option value="Business Class ">Business Class</option>
									<option value="Premium economy Class">Premium economy Class</option>
									<option value="Economy Class">Economy Class</option>
								</select>
						 </td>
						 <td>{{$row['planename']}}</td>
							<td><button  type="submit" value="<?=$row['fid']?>" name="fid" id="fid" class="btn btn-danger">จอง</button></td>

				 </form>

				 </tr>
					 @endforeach
   			</table>
		</div>
	</div>
</div>


    <script>
      function myFunction() {
          var input, filter, table, tr, td, i, txtValue;
          input = document.getElementById("myInput");
          filter = input.value.toUpperCase();
          table = document.getElementById("myTable");
          tr = table.getElementsByTagName("tr");
                for (i = 0; i < tr.length; i++) {
                  td = tr[i].getElementsByTagName("td")[0];
                  if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                      tr[i].style.display = "";
                    } else {
                      tr[i].style.display = "none";
                    }
                  }
                }
      }
    </script>

<script>
    function myFunction1() {
      var input, filter, table, tr, td, i, txtValue;
      input = document.getElementById("myInput1");
      filter = input.value.toUpperCase();
      table = document.getElementById("myTable");
      tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
              td = tr[i].getElementsByTagName("td")[1];
              if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                  tr[i].style.display = "";
                } else {
                  tr[i].style.display = "none";
                }
              }
            }
    }
</script>

<script>
    function myFunction2() {
      var input, filter, table, tr, td, i, txtValue;
      input = document.getElementById("myInput2");
      filter = input.value.toUpperCase();
      table = document.getElementById("myTable");
      tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
              td = tr[i].getElementsByTagName("td")[2];
              if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                  tr[i].style.display = "";
                } else {
                  tr[i].style.display = "none";
                }
              }
            }
    }
</script>

<script>
    function myFunction3() {
      var input, filter, table, tr, td, i, txtValue;
      input = document.getElementById("myInput3");
      filter = input.value.toUpperCase();
      table = document.getElementById("myTable");
      tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
              td = tr[i].getElementsByTagName("td")[3];
              if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                  tr[i].style.display = "";
                } else {
                  tr[i].style.display = "none";
                }
              }
            }
    }
    </script>

@stop
@section('footer')
@stop
