@extends('BoardStaff.masterStaff')
@section('title','Reserve')
@section('head')
@stop
@section('content')
<style>
	*{
        box-sizing: border-box;
      }

      #myInput,#myInput1,#myInput2,#myInput3 {
        background-image: url('/css/searchicon.png');
        background-position: 10px 10px;
        background-repeat: no-repeat;
        width: 15%;
        font-size: 16px;
        padding: 12px 20px 12px 40px;
        border: 1px solid #ddd;
        margin-bottom: 12px;
        margin-left: 10px;
      }

      #myTable {
        border-collapse: collapse;
        width: 100%;
        border: 1px solid #ddd;
        font-size: 18px;
        text-align: center;
      }

      #myTable th, #myTable td {
        text-align: left;
        padding: 12px;
        text-align: center;
      }

      #myTable tr {
        border-bottom: 1px solid #ddd;
      }

      #myTable tr.header, #myTable tr:hover {
        background-color: #f1f1f1;
      }
</style>

<div class="container mt-5 mb-5">
	<h1 class="display-4 mt-5 mb-5">Reserve For Customer</h1>
	<div class="row">
		<div class="col-lg-4">
			<form action="showCustomerByStaff" method="post" class="pt-5 pb-5">
				{{ csrf_field() }}
				<select name = "name1" class="form-control" style="width: 500px;">
					  <option value="0">Number ID Card Customer</option>
					  <?php
					  		$customer = DB::table('customers')->get();
					  		foreach ($customer as $customers) {
                  echo "<option value=".$customers->id_card.">".$customers->id_card."</option>";
					  		}
					  ?>
				</select>

			<a href="{{url('showCustomerByStaff')}}">
				<input type="submit" value="Search" class="btn btn-primary" style= "margin-top: 10px;" id = "showDataCustomer" >
			</a>
    </form>
		</div>
	</div>
</div>

@stop
@section('footer')
@stop
