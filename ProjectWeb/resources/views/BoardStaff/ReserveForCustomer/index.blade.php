@extends('BoardStaff.masterStaff')
@section('title','Reserve')
@section('head')
@stop
@section('content')
<style>
	*{
        box-sizing: border-box;
      }

      #myInput,#myInput1,#myInput2,#myInput3 {
        background-image: url('/css/searchicon.png');
        background-position: 10px 10px;
        background-repeat: no-repeat;
        width: 15%;
        font-size: 16px;
        padding: 12px 20px 12px 40px;
        border: 1px solid #ddd;
        margin-bottom: 12px;
        margin-left: 10px;
      }

      #myTable {
        border-collapse: collapse;
        width: 100%;
        border: 1px solid #ddd;
        font-size: 18px;
        text-align: center;
      }

      #myTable th, #myTable td {
        text-align: left;
        padding: 12px;
        text-align: center;
      }

      #myTable tr {
        border-bottom: 1px solid #ddd;
      }

      #myTable tr.header, #myTable tr:hover {
        background-color: #f1f1f1;
      }
</style>

<div id="content-wrapper">
	<div class="container-fluid">
		<!-- @if(count($errors)>0)
			<div class="aler alert-danger">
				<ul>
				@foreach($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div>
		@endif

		@if(\Session::has('success'))
			<div class="alert alert-success">
			<p>{{\Session::get('success')}}</p>
			</div>
		@endif-->
		<!-- Breadcrumbs-->
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="#">Dashboard</a>
			</li>
			<li class="breadcrumb-item active">Reserve</li>
		</ol>

			<div class="aler alert-danger">
				<ul>

				</ul>
			</div>

		<div class="container mb-5">
			<a href="{{url('ReserveForCustomerCreate')}}" class="btn btn-primary">Add Reserve For Customer</a>
			<h1 class="display-4 mt-5 mb-5">Reserve For Customer</h1>
			<input type="text" id="myInput" onkeyup="myFunction()"
             		placeholder="ID Card" title="Type in a name">
             	<input type="text" id="myInput1" onkeyup="myFunction1()"
             		placeholder="ต้นทาง" title="Type in a name">
				<input type="text" id="myInput2" onkeyup="myFunction2()"
             		placeholder="ปลายทาง" title="Type in a name">
			<div class="row">
				<br>
				<div class="form-row w-100">
					<div class="table-responsive">
						<table class="table table-hover" id = myTable>
							<tr class="header">
								<th>ID User</th>
								<th>Plane</th>
								<th>Source</th>
								<th>Destinetion</th>
								<th>Seat</th>
								<th>Depart</th>
								<th>Return</th>
								<th>Price</th>
								<th>Edit Book ticket</th>
								<th><center>Check in</center></th>
							</tr>

							@foreach ($data as $row)
							<tr>
								<td>{{($row->id_card)}}</td>
								<td>{{($row->planename)}}</td>
								<td>{{($row->source)}}</td>
								<td>{{($row->destinetion)}}</td>
								<td>{{($row->seat)}}</td>
								<td>{{($row->startDate)}}</td>
								<td>{{($row->endDate)}}</td>
								<td>{{($row->price)}}</td>
								
									<td><a href="{{url('ReserveForCustomerEdit')}}">
										<center><button type="submit" class="btn btn-primary">Edit</button></a>
									</td>
									<td><a href="{{url('#')}}">
										<center><button type="submit" class="btn btn-primary">check in</button></center></a>
									</td>
								</tr>

							</td>
							</tr>
							@endforeach

						</table>
					</tbody>
				</div>
			</div>
		</div>
	</div>
			<script type="text/javascript" src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
			<script type="text/javascript" src="{{ asset('bootstrap/js/bootstrap.bundle.js') }}"></script>

			<footer class="sticky-footer">
				<div class="container my-auto">
					<div class="copyright text-center my-auto">
						<span>Board Staff Welcom to Airline</span>
					</div>
				</div>
			</footer>
		</div>

		<script>
			function myFunction() {
				  var input, filter, table, tr, td, i, txtValue;
				  input = document.getElementById("myInput");
				  filter = input.value.toUpperCase();
				  table = document.getElementById("myTable");
				  tr = table.getElementsByTagName("tr");
				        for (i = 0; i < tr.length; i++) {
				          td = tr[i].getElementsByTagName("td")[0];
				          if (td) {
				            txtValue = td.textContent || td.innerText;
				            if (txtValue.toUpperCase().indexOf(filter) > -1) {
				              tr[i].style.display = "";
				            } else {
				              tr[i].style.display = "none";
				            }
				          }
				        }
			}
		</script>

<script>
		function myFunction1() {
		  var input, filter, table, tr, td, i, txtValue;
		  input = document.getElementById("myInput1");
		  filter = input.value.toUpperCase();
		  table = document.getElementById("myTable");
		  tr = table.getElementsByTagName("tr");
		        for (i = 0; i < tr.length; i++) {
		          td = tr[i].getElementsByTagName("td")[2];
		          if (td) {
		            txtValue = td.textContent || td.innerText;
		            if (txtValue.toUpperCase().indexOf(filter) > -1) {
		              tr[i].style.display = "";
		            } else {
		              tr[i].style.display = "none";
		            }
		          }
		        }
		}
</script>

<script>
		function myFunction2() {
		  var input, filter, table, tr, td, i, txtValue;
		  input = document.getElementById("myInput2");
		  filter = input.value.toUpperCase();
		  table = document.getElementById("myTable");
		  tr = table.getElementsByTagName("tr");
		        for (i = 0; i < tr.length; i++) {
		          td = tr[i].getElementsByTagName("td")[3];
		          if (td) {
		            txtValue = td.textContent || td.innerText;
		            if (txtValue.toUpperCase().indexOf(filter) > -1) {
		              tr[i].style.display = "";
		            } else {
		              tr[i].style.display = "none";
		            }
		          }
		        }
		}
</script>

		@stop
		@section('footer')
		@stop
