@extends("master")
@section("title","Edite Customer")
@section('head')
@stop
@section("content")
<div class="container mt-5 mb-5">
	<h1 class="display-4 mt-5 mb-5">Edite Profile</h1>
	<div class="row">
		<div class="col-lg-4">
			<form action="/StaffUpdate" method="post" class="pt-5 pb-5">
				{{ csrf_field() }}
				<div class="form-group">
						<input type="hidden" class="form-control" id="id" placeholder="Enter id" name="id" value="<?= $staff[0]->sid; ?>">
					</div>
				<div class="form-group">
					<label for="idcard">ID Card</label>
					<input type="text" class="form-control" id="idcard" placeholder="ID Card" name="idcard"
					value="<?= $staff[0]->id_card; ?>">
				</div>
				<div class="form-group">
					<label for="password">Password</label>
					<input type="text" class="form-control" id="password" placeholder="Password" name="password" value="<?= $staff[0]->password; ?>">
				</div>
				<div class="form-group">
					<label for="fname">First Name</label>
					<input type="text" class="form-control" id="fname" placeholder="First Name" name="fname" value="<?= $staff[0]->firstname; ?>">
				</div>
				<div class="form-group">
					<label for="lname">Last Name</label>
					<input type="text" class="form-control" id="lname" placeholder="Last Name" name="lname" value="<?= $staff[0]->lastname; ?>">
				</div>
				<div class="form-group">
					<label for="Sex">Sex</label>
					<input type="text" class="form-control" id="Sex" placeholder="Sex" name="Sex" value="<?= $staff[0]->sex; ?>">
				</div>
				<div class="form-group">
					<label for="Address">Address</label>
					<input type="text" class="form-control" id="Address" placeholder="Address" name="Address" value="<?= $staff[0]->address; ?>">
				</div>
				<div class="form-group">
					<label for="Email">Email</label>
					<input type="text" class="form-control" id="Email" placeholder="Email" name="Email" value="<?= $staff[0]->email; ?>">
				</div>
				<div class="form-group">
					<label for="phone">Phone</label>
					<input type="text" class="form-control" id="phone" placeholder="Phone" name="phone" value="<?= $staff[0]->phone; ?>">
				</div>
				<div class="form-group">
					<label for="phone">Position</label>
					<input type="text" class="form-control" id="position" placeholder="Position" name="position" value="<?= $staff[0]->position; ?>">
				</div>
				<div class="form-group">
					<!-- <input type="hidden" name="_method" value="PATCH"/> -->
					<button type="submit" class="btn btn-primary"> Update </button>
				</div>
			</form>
		</div>
	</div>
</div>
@stop
@section("footer")
@stop
