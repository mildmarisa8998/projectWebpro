
@extends('master')
@section('title','Add Staff')
@section('head')
@stop
@section('content')
<div class="container mt-5 mb-5">
	<h1 class="display-4 mt-5 mb-5">Add Staff</h1>
	<div class="row">
		<div class="col-lg-4">
			<form action="{{url('Staff')}}" method="post" class="pt-5 pb-5">
				{{ csrf_field() }}
				<div class="form-group">
					<label for="idcard">ID Card</label>
					<input type="text" class="form-control" id="idcard" placeholder="ID Card" name="idcard">
				</div>
				<div class="form-group">
					<label for="password">Password</label>
					<input type="text" class="form-control" id="password" placeholder="Password" name="password">
				</div>
				<div class="form-group">
					<label for="fname">First Name</label>
					<input type="text" class="form-control" id="fname" placeholder="First Name" name="fname">
				</div>
				<div class="form-group">
					<label for="lname">Last Name</label>
					<input type="text" class="form-control" id="lname" placeholder="Last Name" name="lname">
				</div>
				<div class="form-group">
					<label for="Sex">Sex</label>
					<input type="text" class="form-control" id="Sex" placeholder="Sex" name="Sex">
				</div>
				<div class="form-group">
					<label for="Address">Address</label>
					<input type="text" class="form-control" id="Address" placeholder="Address" name="Address">
				</div>
				<div class="form-group">
					<label for="Email">Email</label>
					<input type="text" class="form-control" id="Email" placeholder="Email" name="Email">
				</div>
				<div class="form-group">
					<label for="phone">Phone</label>
					<input type="text" class="form-control" id="phone" placeholder="Phone" name="phone">
				</div>
				<div class="form-group">
					<label for="position">Position</label>
					<input type="text" class="form-control" id="position" placeholder="position" name="position">
				</div>
				<button type="add" class="btn btn-primary">  Add Staff  </button>
			</form>
		</div>
	</div>
</div>
@stop
@section('footer')
@stop
