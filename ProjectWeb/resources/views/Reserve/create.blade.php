@extends('master')
@section('title','Add Customer')
@section('head')
@stop
@section('content')
<div id="content-wrapper">

	<div class="container-fluid">

		<!-- Breadcrumbs-->
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="#">Dashboard</a>
			</li>
			<li class="breadcrumb-item active">Customer</li>
		</ol>

<div class="container mt-5 mb-5">
	<h1 class="display-4 mt-5 mb-5">Add Customer</h1>
	<div class="row">
		<div class="col-lg-4">
		<!-- @if(count($errors)>0)
			<div class="aler alert-danger">
				<ul>
				@foreach($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div>
		@endif

		@if(\Session::has('success'))
			<div class="alert alert-success">
			<p>{{\Session::get('success')}}</p>
			</div>
		@endif-->
			<form action="{{url('Customer')}}" method="post" class="pt-5 pb-5">
				{{ csrf_field() }}
				<div class="form-group">
					<label for="idcard">ID Card</label>
					<input type="text" class="form-control" id="idcard" placeholder="ID Card" name="idcard">
				</div>
				<div class="form-group">
					<label for="password">Password</label>
					<input type="text" class="form-control" id="password" placeholder="Password" name="password">
				</div>
				<div class="form-group">
					<label for="fname">First Name</label>
					<input type="text" class="form-control" id="fname" placeholder="First Name" name="fname">
				</div>
				<div class="form-group">
					<label for="lname">Last Name</label>
					<input type="text" class="form-control" id="lname" placeholder="Last Name" name="lname">
				</div>
				<div class="form-group">
					<label for="Sex">Sex</label>
					<input type="text" class="form-control" id="Sex" placeholder="Sex" name="Sex">
				</div>
				<div class="form-group">
					<label for="Address">Address</label>
					<input type="text" class="form-control" id="Address" placeholder="Address" name="Address">
				</div>
				<div class="form-group">
					<label for="Email">Email</label>
					<input type="text" class="form-control" id="Email" placeholder="Email" name="Email">
				</div>
				<div class="form-group">
					<label for="phone">Phone</label>
					<input type="text" class="form-control" id="phone" placeholder="Phone" name="phone">
				</div>
				<button type="submit" class="btn btn-primary">  Add Customer  </button></a>
			</form>
		</div>
	</div>
</div>
</div>
@stop
@section('footer')
@stop
