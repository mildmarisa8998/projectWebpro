@extends('master')
@section('title','Reserve')
@section('head')
@stop
@section('content')
<div id="content-wrapper">

	<div class="container-fluid">
		<!-- @if(count($errors)>0)
			<div class="aler alert-danger">
				<ul>
				@foreach($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div>
		@endif

		@if(\Session::has('success'))
			<div class="alert alert-success">
			<p>{{\Session::get('success')}}</p>
			</div>
		@endif-->
		<!-- Breadcrumbs-->
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="#">Dashboard</a>
			</li>
			<li class="breadcrumb-item active">Reserve</li>
		</ol>
		@if(count($errors)>0)
			<div class="aler alert-danger">
				<ul>
				@foreach($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div>
		@endif
		<div class="container mb-5">
			<!-- <a href="{{route('Customer.create')}}" class="btn btn-primary">Add Customer</a> -->
			<h1 class="display-4 mt-5 mb-5">Reserve</h1>
			<div class="row">
				<div class="form-row w-100">
					<div class="table-responsive">
						<table class="table table-hover">
							<tr>
								<th>ID User</th>
								<th>Plane</th>
								<th>Source</th>
								<th>Destinetion</th>
								<th>Seat</th>
								<th>Depart</th>
								<th>Return</th>
								<th>Price</th>
								<th></th>
								<th></th>
							</tr>
							@foreach ($data as $row)
							<tr>
								<td>{{($row->id_card)}}</td>
								<td>{{($row->planename)}}</td>
								<td>{{($row->source)}}</td>
								<td>{{($row->destinetion)}}</td>
								<td>{{($row->seat)}}</td>
								<td>{{($row->startDate)}}</td>
								<td>{{($row->endDate)}}</td>
								<td>{{($row->price)}}</td>
						
							</tr>
							@endforeach
						</table>
					</tbody>
				</div>
			</div>
		</div>
	</div>
			<script type="text/javascript" src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
			<script type="text/javascript" src="{{ asset('bootstrap/js/bootstrap.bundle.js') }}"></script>

			<footer class="sticky-footer">
				<div class="container my-auto">
					<div class="copyright text-center my-auto">
						<span>Welcom to Airline</span>
					</div>
				</div>
			</footer>

		</div>
		@stop
		@section('footer')
		@stop
