<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>2M Airline</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="" />
<meta name="author" content="http://webthemez.com" />
<!-- css -->
<link href="css/bootstrap.min.css" rel="stylesheet" />
<link href="css/fancybox/jquery.fancybox.css" rel="stylesheet">
<link href="css/jcarousel.css" rel="stylesheet" />
<link href="css/flexslider.css" rel="stylesheet" />
<link href="js/owl-carousel/owl.carousel.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet" />
 

</head>
<body>
<div id="wrapper" class="home-page">
<div class="topbar">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <p class="pull-left hidden-xs">Welcome</p>
        <p class="pull-right"><i class="fa fa-phone"></i></p>
      </div>
    </div>
  </div>
</div>
	<!-- start header -->
	<header>
        <div class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="index.php">Home</a></li> 
						<li><a href="register.php">Login</a></li>
                        <li><a href="register.php">Register</a></li>
                    </ul>
                </div>

                <div class="navbar-collapse collapse ">
                    
                </div>

            </div>
        </div>

	</header>
	<!-- end header -->
	<section id="banner">
	 
	<!-- Slider -->
        <div id="main-slider" class="flexslider">
            <ul class="slides">
              <li>
                <img src="img/slides/3.jpg" alt="" />
              </li>
              <li>
                <img src="img/slides/4.jpg" alt="" />
              </li>
               <li>
                <img src="img/slides/5.jpg" alt="" />
              </li>
               <li>
                <img src="img/slides/6.jpg" alt="" />
              </li>
            </ul>
        </div>
	<!-- end slider -->
 
	</section> 
 
	
	<section id="content">
	<div class="container"> 
			<div class="row">
		<div class="skill-home"> <div class="skill-home-solid clearfix"> 
		<div class="col-md-3 col-sm-6 text-center">
		<span class="icons c1"><i class="fa fa-home"></i></span> <div class="box-area">
		<h3>ราคาถูกที่สุดของแต่ละวัน</h3> <p>จองตั๋วเครื่องบินราคาถูกแบบไม่มีค่าธรรมเนียม จ่ายเท่าที่เห็น แถมเปรียบเทียบราคาเที่ยวบินต่างๆบนผลลัพธ์การค้นหา</p>
		<img src="img/icon1.jpg" alt="" />
		</div>
		</div>
		
		<div class="col-md-3 col-sm-6 text-center"> 
		<span class="icons c2"><i class="fa fa-rocket"></i></span> <div class="box-area">
		<h3>ส่วนลดพิเศษ</h3> <p>รับส่วนลดพิเศษสำหรับการจองตั๋วเครื่องบิน สมัครรับข่าวสาร หรือสมัครสมาชิก มีส่วนลดพิเศษ<br><br></p>
		<img src="img/icon2.jpg" alt="" />
		</div>
		</div>

		<div class="col-md-3 col-sm-6 text-center"> 
		<span class="icons c3"><i class="fa fa-trophy"></i></span> <div class="box-area">
		<h3>รับประกันราคาถูก แถมประหยัดเพิ่มเมื่อเป็นสมาชิก</h3> <p>สมัคสมาชิก 2M airline เลย เพื่อจองในราคาสุดประหยัด พร้อมรับโปรโมชั่นสุดฮอต!</p>
		<img src="img/icon3.jpg" alt="" />
		</div>
		</div>	

		<div class="col-md-3 col-sm-6 text-center"> 
		<span class="icons c4"><i class="fa fa-star"></i></span> <div class="box-area">
		<h3>ช่องทางชำระที่หลากหลาย</h3> <p>ชำระเงินได้ง่ายดายผ่านหลากหลายช่องทาง ไม่ว่าจะเป็นเคาน์เตอร์ชำระเงิน การโอนเงิน ATM บัตรเครดิต และอินเตอร์เน็ตแบงก์กิ้ง</p>
		<img src="img/icon4.jpg" alt="" />
		</div>
		</div>
		
		</div></div>
		</div> 
	</div>
	
	<section class="section-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="section-title text-center">
						<h2>ชั้นโดยสารของ สายการบิน และอาหาร</h2>
					</div>
				</div>
			</div>
			<div class="row service-v1 margin-bottom-40">
            <div class="col-md-3 col-sm-6 md-margin-bottom-40">
               <img class="img-responsive" src="img/img1.jpg" alt="">   
                <h3>ที่นั่งชั้นประหยัดมาตรฐาน</h3>
                <p>ห้บริการที่นั่งชั้นประหยัดในทุกเที่ยวบินและเครื่องบินทุกรุ่น ที่นั่งของเราบุหนังพร้อมกับมีที่ว่างเท้ากว้างขวาง เพื่อมอบความสะดวกสบายให้ผู้โดยสารทุกคนบนเที่ยวบินชั้นประหยัด</p>    
            </div>
            <div class="col-md-3 col-sm-6 md-margin-bottom-40">
                <img class="img-responsive" src="img/img2.jpg" alt="">            
                <h3>โซนปลอดเสียง</h3>
                <p>สายการยินทราบดีว่าผู้โดยสารทุกคนต่างต้องการความเงียบสงบระหว่างเดินทาง ด้วยเหตุนี้ แอร์เอเชียจึงได้เริ่มให้บริการโซนปลอดเสียงตั้งแต่เดือนกุมภาพันธ์ 2013 เพื่อรองรับความต้องการตรงส่วนนี้ โซนปลอดเสียงตั้งอยู่ตรงแถวที่ 7 ถึง 14 และเปิดให้จองเป็นพิเศษระหว่างที่จองที่นั่ง หรือกำลังเช็คอินเท่านั้น สำหรับผู้โดยสารที่เดินทางพร้อมกับผู้เดินทางอายุน้อยกว่า 10 ปี (ไม่ว่าจะเป็นเด็กหรือทารก) จะไม่สามารถจองที่นั่งในโซนปลอดเสียงได้</p>          
            </div>
            <div class="col-md-3 col-sm-6 md-margin-bottom-40">
              <img class="img-responsive" src="img/img3.jpg" alt="">  
                <h3>ที่นั่งพรีเมี่ยมแฟลตเบด</h3>
                <p>ห้บริการที่นั่งพรีเมี่ยมแฟลตเบด เพื่อมอบความสะดวกสบายเป็นพิเศษให้แก่ท่านผู้โดยสารที่เดินทางไกล ดังนั้นที่นั่งพรีเมี่ยมแฟลตเบดจึงมีเฉพาะบนสายการบิน ผู้โดยสารสามารถปรับที่นั่งอันกว้างขวางนี้ให้เป็นเตียงนอนได้ตามที่ต้องการ ที่นั่งพรีเมี่ยมแผลตเบตยังมาพร้อมกับจอภาพส่วนตัว ที่พิงศีรษะและที่วางเท้าที่ปรับได้ รวมถึงหมอนและผ้าห่ม เพื่อความสะดวกสบายสูงสุดของด้วย นอกจากนี้ ยังมีไฟสำหรับอ่านหนังสือและบริการปลั๊กไฟแบบสากลด้วย</p>    
            </div> 
			    <div class="col-md-3 col-sm-6 md-margin-bottom-40">
               <img class="img-responsive" src="img/img4.jpg" alt="">   
                <h3>อาหารบนเครื่อง</h3>
               <p>ได้รับบริการอาหารฟรีบนเครื่อง แต่สามารถเลือกสั่งเมนูที่ต้องการบนเครื่อง </p>    
            </div> 
        </div>
		</div>
		</section>
	
	
	</section>
	
		<section class="section-padding gray-bg">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="section-title text-center">
						<h2>รายละเอียดต่างๆที่ควรทราบ</h2>
						<p>*ลูกค้าต้องทำการ login หรือ สมัครสมาชิก ก่อนจึงจะสามารถจองตั๋วเครื่องบิน 2M airline ได้
					</div>
				</div>
			</div>
			<div class="row service-v1 margin-bottom-40">
            <div class="col-md-4 md-margin-bottom-40">
               <img class="img-responsive" src="img/img5.jpg" alt="">   
                <h3>กระเป๋าสัมภาระ</h3>
                <p>Lท่านสามารถนำกระเป๋าสัมภาระพกพาขึ้นบนห้องโดยสารได้เพียง 1 ใบเท่านั้นโดย ต้องมีขนาดไม่เกิน 56 ซม. x 36 ซม. x 23 ซม. เมื่อรวมกับด้ามจับ ล้อ และกระเป๋าด้านข้างแล้ว ทั้งนี้ กระเป๋าสัมภาระต้องสามารถนำขึ้นเก็บไว้ในช่องเก็บของเหนือศีรษะได้ และกระเป๋าขนาดเล็กอีก 1 ใบ </p>        
            </div>
            <div class="col-md-4 md-margin-bottom-40">
                <img class="img-responsive" src="img/img6.jpg" alt="">            
                <h3>กระเป๋าสัมภาระพกพา</h3>
                <p>ท่านสามารถพกพากระเป๋าใส่คอมพิวเตอร์โน้ตบุ๊ก กระเป๋าถือ เป้สะพายหลัง หรือกระเป๋าขนาดเล็กที่มีขนาดไม่เกิน 40 ซม. X 30 ซม. X 10 ซม. ขึ้นห้องโดยสารได้อีก 1 ใบโดยกระเป๋าดังกล่าวต้องมีขนาดที่สามารถจัดเก็บไว้ใต้ที่นั่งด้านหน้าของท่านได้ น้ำหนักรวมของสัมภาระสอง (2) ชิ้นต้องไม่เกิน 7 กก.. หมายเหตุ: สิ่งของหลายชิ้นที่นำมารัด ห่อ หรือมัดเข้าด้วยกันจะไม่นับว่าเป็นสัมภาระพกพา 1 ชิ้น</p>        
            </div>
            <div class="col-md-4 md-margin-bottom-40">
              <img class="img-responsive" src="img/img7.jpg" alt="">  
                <h3>บริการความบันเทิงระหว่างเที่ยวบิน</h3>
                <p>ให้บริการความบันเทิงระหว่างเที่ยวบิน Xcite ผ่านทางอุปกรณ์แท็บเล็ต Xcite Tabs โดยผู้โดยสารสามารถเลือกรับชมสิ่งที่น่าสนใจที่สายการบินคัดสรรมาให้ผ่าน Xcite Tabs ที่มีทั้งภาพยนตร์ ทีวีซีรี่ส์ ดนตรี นิตยสาร และเกมส์ และเพื่อให้ได้รับประสบการณ์ความสนุกอย่างไร้พรมแดน บริการ Xcite จึงออกแบบมาให้แสดงผลใน 5 ภาษา คือ ภาษาอังกฤษ มาเลย์ จีน เกาหลี และญี่ปุ่น นอกจากนี้ Xcite Tabs ยังมีชุดหูฟังให้อีกด้วย</p>        
            </div>
			</div>
			<div class="row service-v1 margin-bottom-40">
			    <div class="col-md-4 md-margin-bottom-40">
               <img class="img-responsive" src="img/img8.jpg" alt="">   
                <h3>เช็คอินด้วยตัวเอง</h3>
                <p>มื่อท่านจองตั๋วเครื่องบินและได้รับตั๋วเรียบร้อยแล้ว เพื่อหลีกเลี่ยงการต่อแถวเช็คอินที่เคาน์เตอร์ด้วยบริการเช็คอินด้วยตัวเอง! คุณสามารถเช็คอินผ่านเว็บไซต์ มือถือ หรือตู้คีออสได้อย่างสะดวกและรวดเร็วมากขึ้น</p>        
            </div>
            <div class="col-md-4 md-margin-bottom-40">
                <img class="img-responsive" src="img/img9.jpg" alt="">            
                <h3>เคาน์เตอร์เช็คอิน</h3>
                <p>ข้อกำหนดการให้บริการเคาน์เตอร์เช็คอินเปิดให้บริการสำหรับทุกเที่ยวบินของแอร์เอเชีย เพียงไปที่สนามบินพร้อมกับเอกสารการเดินทางของคุณ แล้วเช็คอินผ่านเคาน์เตอร์ของเราพร้อมรับบัตรขึ้นเครื่อง
                </p>        
            </div>
            <div class="col-md-4 md-margin-bottom-40">
              <img class="img-responsive" src="img/img10.jpg" alt="">  
                <h3>ค่าธรรมเนียมการเช็คอินผ่านเคาน์เตอร์ </h3>
                <p>มีการเรียกเก็บ ค่าธรรมเนียม สำหรับผู้โดยสารที่เช็คอินเที่ยวบินภายในประเทศมาเลเซียผ่านเคาน์เตอร์ เพื่อหลีกเลี่ยงค่าธรรมเนียมที่จะเกิดขึ้น แอร์เอเชียขอแนะนำให้คุณดำเนินการเช็คอินด้วยตัวเอง กำหนดเวลาการให้บริการเช็คอินอาจแตกต่างกันไปตามท่าอากาศยานแต่ละแห่ง สนามบินมีคิวยาวเนื่องจากการตรวจสอบความปลอดภัยที่ละเอียดถี่ถ้วน เราขอแนะนำให้คุณเผื่อเวลาเช็คอินล่วงหน้าและตรงไปยังประตูทางออกขึ้นเครื่องทันที</p>        
            </div>
        </div>
		</div>
		</section>
		<section class="section-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="section-title text-center">
						<h2>โปรดเข้าสู่ระบบ หรือสมัครสมาชิก เพื่อรับสิทธิประโยชน์มากมาย</h2>
						<p>นโยบายของสายการบิน เพื่อความสะดวก และเพื่อรักษาสิทธิและโยชน์ของท่านผู้โดยสาร โปรดกรอกข้อมูลโดยความเป็นจริง โดยข้อมูลของท่านจะถูกเก็บเป็นความลับของบริษัท </p>
					</div>
				</div>
			</div>
			 
			<div class="row">

				<div class="col-md-6 col-sm-6">
					<div class="about-text">
						<ul class="withArrow">
							<li><span class="fa fa-angle-right"></span> สมัครสมาชิก / เข้าสู่ระบบ</li>
							<li><span class="fa fa-angle-right"></span> เลือกเที่ยวบิน และที่นั่งที่ต้องการ</li>
							<li><span class="fa fa-angle-right"></span> ยืนยันการชำระเงิน</li>
							<li><span class="fa fa-angle-right"></span> เตรียมตัวออกเดินทางตามวันเวลาที่กำหนด</li>
						</ul>
						<a href="#" class="btn btn-primary">เข้าสู่ระบบ</a>
						<a href="#" class="btn btn-primary">สมัครสมาชิก</a>
					</div>
				</div>

				<div class="col-md-6 col-sm-6">
					<img src="img/about.jpg" alt="About Images">	
				</div>

			</div>
		</div>
	</section>	  
	
				
					

	<footer>	

		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="copyright">
						<p>
							<span>&copy;Template By </span>WebThemez</a>
						</p>
					</div>
				</div>
			</div>
		</div>

	</footer>
</div>
<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.fancybox.pack.js"></script>
<script src="js/jquery.fancybox-media.js"></script>  
<script src="js/jquery.flexslider.js"></script>
<script src="js/animate.js"></script>
<!-- Vendor Scripts -->
<script src="js/modernizr.custom.js"></script>
<script src="js/jquery.isotope.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/animate.js"></script>
<script src="js/custom.js"></script>
<script src="js/owl-carousel/owl.carousel.js"></script>
</body>
</html>