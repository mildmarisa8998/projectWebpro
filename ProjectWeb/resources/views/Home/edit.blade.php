<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>EDIT PROFILE</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- Plugin CSS -->
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="css/freelancer.min.css" rel="stylesheet">
    <link href="css/flexslider.css" rel="stylesheet">

    <link rel="stylesheet" href="/path/to/bootstrap.min.css">
    <script src="/path/to/jquery.min.js"></script>
    <script src="ccFileUpload.js"></script>



  </head>

  <body id="page-top">
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg bg-secondary fixed-top text-uppercase" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">Edit Profile</a>
        <button class="navbar-toggler navbar-toggler-right text-uppercase bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fas fa-bars"></i>
        </button>

          <div class="dropdown" style="margin: 15px">
            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Welcome : {{($data[0]->firstname)}} <span class="caret"></span>
            </button>

            <ul class="dropdown-menu">
                  <li><a href="#">แก้ไขข้อมูลส่วนตัว</a></li>
                  <li><a href="#">ออกจากระบบ</a></li>
            </ul>
         </div>
      </div>
    </nav>

    <br><br>
   <section id="create">
        <h2 class="text-center text-uppercase text-secondary mb-0">Edit Profile</h2>
        <hr class="star-dark mb-5">
        <center>
        <form  name="register" action="/CustomerHomeUpdate" method="POST" class="form-horizontal">
          {{csrf_field()}}
             <div class="form-group">
                <div class="col-sm-5" align="left">
                  <input  name="firstname" type="text" required class="form-control" id="firstname" placeholder="firstname" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2"
                   value="<?=$data[0]->firstname; ?>"/>
                </div>
            </div>
              <div class="form-group">
                <div class="col-sm-5" align="left">
                  <input  name="lastname" type="text" required class="form-control" id="lastname" placeholder="lastname" minlength="2" value="<?= $data[0]->lastname; ?>"/>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-5" align="left">
                  <input  name="idcard" type="text" required class="form-control" id="idcard" placeholder="ID  " value="<?= $data[0]->cid; ?>" />
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-5" align="left">
                  <input  name="sex" type="text" required class="form-control" id="idcard" placeholder="เพศ  " value="<?= $data[0]->sex; ?>" />
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-5" align="left">
                  <input  name="phone" type="text" class="form-control" id="phone"   placeholder="เบอร์โทร ตัวเลขเท่านั้น" value="<?= $data[0]->phone; ?>"/>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-5" align="left">
                  <input  name="email" type="text" class="form-control" id="email"  placeholder="อีเมล์ name@hotmail.com" value="<?= $data[0]->email; ?>"/>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-5" align="left">
                  <input  name="address" type="text" class="form-control" id="address"  placeholder="ที่อยู่" value="<?= $data[0]->address; ?>" />
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-5" align="left">
                  <input  name="password" type="text" class="form-control" id="password"  placeholder="รหัสผ่าน"  value="<?= $data[0]->password; ?>"/>
                </div>
              </div>
              <div class="form-group">
                  <input type="hidden" class="form-control" id="id" placeholder="Enter id" name="id" value="<?= $data[0]->cid; ?>">
                </div>
            <div class="form-group">
                <div class="col-sm-5" align="right"> <center>
                <button type="submit" class="btn btn-success" id="btn"><span class="glyphicon glyphicon-ok"></span> ยืนยันการแก้ไข  </button></center>
                </div>   <!--go customer-->
            </div>
            </form>
            </center>
      </section>


    <!-- Footer -->
    <br>
    <br><br><br><br>
    <footer class="footer text-center" style="margin-bottom: 0">
      <div class="container">
        <div class="row">
          <div class="col-md-4 mb-5 mb-lg-0">
            <h4 class="text-uppercase mb-4">Location</h4>
            <p class="lead mb-0"> 2M airline @ Mahasarakham 1234</p>
          </div>
          <div class="col-md-4 mb-5 mb-lg-0">
            <h4 class="text-uppercase mb-4">Welcome to 2M Airline</h4>
            <ul class="list-inline mb-0">
              <li class="list-inline-item">
                <a class="btn btn-outline-light btn-social text-center rounded-circle" href="#">
                  <i class="fab fa-fw fa-facebook-f"></i>
                </a>
              </li>

               <li class="list-inline-item">
                <a class="btn btn-outline-light btn-social text-center rounded-circle" href="#">
                  <i class="fab fa-fw fa-facebook-f"></i>
                </a>
              </li>

            </ul>
          </div>
          <div class="col-md-4">
            <h4 class="text-uppercase mb-4">About</h4>
            <p class="lead mb-0">Project Term Web Programming 3CS 1/2561 @computer Science mahasarakham university thailand
          </div>
        </div>
      </div>
    </footer>


    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-to-top d-lg-none position-fixed ">
      <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top">
        <i class="fa fa-chevron-up"></i>
      </a>
    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/freelancer.min.js"></script>


<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script language="javascript">
    $('.dropdown-toggle').dropdown();
    $('.dropdown-menu').find('form').click(function (e) {
        e.stopPropagation();
      });
</script>
  </body>

</html>
