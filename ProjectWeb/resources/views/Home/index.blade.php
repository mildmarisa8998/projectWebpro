<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>2M Airline</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- Plugin CSS -->
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="css/freelancer.min.css" rel="stylesheet">
     <link href="css/flexslider.css" rel="stylesheet">
     <style>
          * {
            box-sizing: border-box;
          }

          #myInput,#myInput1,#myInput2,#myInput3 {
            background-image: url('/css/searchicon.png');
            background-position: 10px 10px;
            background-repeat: no-repeat;
            width: 15%;
            font-size: 16px;
            padding: 12px 20px 12px 40px;
            border: 1px solid #ddd;
            margin-bottom: 12px;
            margin-left: 10px;
          }

          #myTable {
            border-collapse: collapse;
            width: 100%;
            border: 1px solid #ddd;
            font-size: 18px;
            text-align: center;
          }

          #myTable th, #myTable td {
            text-align: left;
            padding: 12px;
            text-align: center;
          }

          #myTable tr {
            border-bottom: 1px solid #ddd;
          }

          #myTable tr.header, #myTable tr:hover {
            background-color: #f1f1f1;
          }
    </style>

  </head>
  <body id="page-top">
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg bg-secondary fixed-top text-uppercase" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">2M Airline</a>
        <button class="navbar-toggler navbar-toggler-right text-uppercase bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#portfolio">Promotion</a>
            </li>
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#about">Eclusive</a>
            </li>
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#create">create new account</a>
            </li>
          </ul>
        </div>

          <div class="dropdown" style="margin: 15px">
            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Login<span class="caret"></span>
            </button>

             <div class="dropdown-menu">
               <form name="Loing" style="margin: 0px" accept-charset="UTF-8" action="/logincheckCustomer" method="POST" class="form-horizontal">
               {{csrf_field()}}

                <div style="margin:0;padding:0;display:inline">
                  <input name="utf8" type="hidden" value="&#x2713;" />
                  <input name="authenticity_token" type="hidden" value="4L/A2ZMYkhTD3IiNDMTuB/fhPRvyCNGEsaZocUUpw40=" />
                </div>
                 <fieldset class='textbox' style="padding:10px">
                   <input style="margin-top: 8px" name ="email" id = email type="text" placeholder="E-mail" />
                   <input style="margin-top: 8px" name ="password" id = password type="password" placeholder="Passsword" />
                   <input style="margin-top: 8px" class="btn-primary" name="commit" type="submit" value="Log In" />
                 </fieldset>
               </form>
             </div>
         </div>
      </div>
    </nav>

    <!-- Header -->
    <header class="masthead bg-primary text-white text-center">
      <div class="container">
        <h1 class="text-uppercase mb-0">
            Let us take care of you</h1>
      </div>
    </header>


    <!-- Portfolio Grid Section -->

    <section class="portfolio" id="portfolio">
      <center>
      <div class="form-group">
           <div class="col-sm-5" align="left">

          @if(count($errors)>0)
           <div class="aler alert-danger">
             <ul>@foreach($errors->all() as $error)
               <li>{{$error}}</li>
             @endforeach
             </ul>
           </div>
         @endif

         @if(\Session::has('success'))
           <div class="alert alert-success">
             <p>{{\Session::get('success')}}</p>
             </div>
           @endif
         </div>
     </div>
   </center>
   <!-- Portfolio Grid Section -->
<section class="portfolio" id="portfolio">
<div class="container">
  <h2 class="text-center text-uppercase text-secondary mb-0">Flight</h2>
  <hr class="star-dark mb-5">
<center>
  <input type="text" id="myInput" onkeyup="myFunction()"
    placeholder="ต้นทาง" title="Type in a name">

  <input type="text" id="myInput1" onkeyup="myFunction1()"
    placeholder="ปลายทาง" title="Type in a name">

    <input type="text" id="myInput2" onkeyup="myFunction2()"
    placeholder="วันออกเดินทาง" title="Type in a name">

    <input type="text" id="myInput3" onkeyup="myFunction3()"
    placeholder="วันกลับ" title="Type in a name">
</center>
    <table id="myTable">
        <tr class="header">
          <th>Source</th>
          <th>Destinetion</th>
          <th>Depart</th>
          <th>Return</th>
          <th>Price</th>
          <th>Plane</th>
        </tr>
        @foreach($flight as $row)
        <tr>
          <td>{{$row['planename']}}</td>
          <td>{{$row['source']}}</td>
          <td>{{$row['destinetion']}}</td>
          <td>{{$row['startDate']}}</td>
          <td>{{$row['endDate']}}</td>
          <td>{{$row['price']}}</td>
        </tr>
        @endforeach
    </table>
    </div>
</section>
    <!-- About Section -->
    <section class="bg-primary text-white mb-0" id="about">
      <div class="container">
        <h2 class="text-center text-uppercase text-white">Eclusive</h2>
        <hr class="star-light mb-5">

        <div class="row">
          <div class="col-lg-4 ml-auto">
              <img src="/img/img1.jpg" style="height: 290px">
          </div>

          <div class="col-lg-4 ml-auto">
            <center><h3>ที่นั่งชั้นประหยัดมาตรฐาน</h3></center>
            <p class="lead">
                ให้บริการที่นั่งชั้นประหยัดในทุกเที่ยวบินและเครื่องบินทุกรุ่น ที่นั่งของเราบุหนังพร้อมกับมีที่ว่างเท้ากว้างขวาง เพื่อมอบความสะดวกสบายให้ผู้โดยสารทุกคนบนเที่ยวบินชั้นประหยัด
                ที่นั่งชั้น Hot Seat จะตั้งอยู่ในแถวที่ 1 ถึง 5 ถัดจากประตูทางออกฉุกเฉินในแถวที่ 12 กับ 14 นอกจากผู้โดยสารของที่นั่ง Hot Seat จะมีที่วางขาที่กว้างกว่าเดิม
            </p>
          </div>
        </div>

          <div class="row" style="margin-top: 50px">

            <div class="col-lg-4 ml-auto">
              <br><br>
              <img src="/img/img2.jpg" style="height: 290px">
            </div>

          <div class="col-lg-4 ml-auto">
            <center><h3>โซนปลอดเสียง</h3></center>
            <p class="lead">
              สายการยินทราบดีว่าผู้โดยสารทุกคนต่างต้องการความเงียบสงบระหว่างเดินทาง ด้วยเหตุนี้ แอร์เอเชียจึงได้เริ่มให้บริการโซนปลอดเสียงตั้งแต่เดือนกุมภาพันธ์ 2013 เพื่อรองรับความต้องการตรงส่วนนี้ โซนปลอดเสียงตั้งอยู่ตรงแถวที่ 7 ถึง 14 และเปิดให้จองเป็นพิเศษระหว่างที่จองที่นั่ง หรือกำลังเช็คอินเท่านั้น สำหรับผู้โดยสารที่เดินทางพร้อมกับผู้เดินทางอายุน้อยกว่า 10 ปี (ไม่ว่าจะเป็นเด็กหรือทารก) จะไม่สามารถจองที่นั่งในโซนปลอดเสียงได้
            </p>
          </div>

          <div class="row" style="margin-top: 50px">
            <div class="col-lg-4 ml-auto">
               <br><br>
              <img src="/img/img3.jpg" style="height: 290px">
            </div>
          <div class="col-lg-4 ml-auto">
            <center><h3>ที่นั่งพรีเมี่ยมแฟลตเบด</h3></center>
            <p class="lead">
              ให้บริการที่นั่งพรีเมี่ยมแฟลตเบด เพื่อมอบความสะดวกสบายเป็นพิเศษให้แก่ท่านผู้โดยสารที่เดินทางไกล ดังนั้นที่นั่งพรีเมี่ยมแฟลตเบดจึงมีเฉพาะบนสายการบินแอร์เอเชีย เอ็กซ์ (AirAsia X) เท่านั้น ผู้โดยสารสามารถปรับที่นั่งอันกว้างขวางนี้ให้เป็นเตียงนอนได้ตามที่ต้องการ ที่นั่งพรีเมี่ยมแผลตเบตยังมาพร้อมกับจอภาพส่วนตัว ที่พิงศีรษะและที่วางเท้าที่ปรับได้ รวมถึงหมอนและผ้าห่ม เพื่อความสะดวกสบายสูงสุดของด้วย นอกจากนี้ ยังมีไฟสำหรับอ่านหนังสือและบริการปลั๊กไฟแบบสากลด้วย
            </p>
          </div>

        </div>

      </div>
    </section>

    <section id="create">
        <h2 class="text-center text-uppercase text-secondary mb-0">create new account</h2>
        <hr class="star-dark mb-5">
        <center>

        <form  name="register" action="{{action('HomeController@store')}}" method="POST" class="form-horizontal">
          {{csrf_field()}}

             <div class="form-group">
                <div class="col-sm-5" align="left">
                  <input  name="firstname" type="text" required class="form-control" id="firstname" placeholder="firstname" pattern="^[a-zA-Z0-9]+$" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2"  />
                </div>
            </div>
              <div class="form-group">
                <div class="col-sm-5" align="left">
                  <input  name="lastname" type="text" required class="form-control" id="lastname" placeholder="lastname" pattern="^[a-zA-Z0-9]+$" minlength="2" />
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-5" align="left">
                  <input  name="idcard" type="text" required class="form-control" id="idcard" placeholder="idcard " />
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-5" align="left">
                  <input  name="Sex" type="text" class="form-control" id="Sex"   placeholder="เพศ"/>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-5" align="left">
                  <input  name="phone" type="text" class="form-control" id="phone"   placeholder="เบอร์โทร ตัวเลขเท่านั้น"/>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-5" align="left">
                  <input  name="email" type="text" class="form-control" id="email"  placeholder="อีเมล์ name@hotmail.com" />
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-5" align="left">
                  <input  name="adress" type="text" class="form-control" id="adress"  placeholder="ที่อยู่" />
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-5" align="left">
                  <input  name="password" type="text" class="form-control" id="password"  placeholder="รหัสผ่าน" />
                </div>
              </div>

            <div class="form-group">

                <div class="col-sm-5" align="right">
                <center><button type="submit" class="btn btn-success" id="btn"><span class="glyphicon glyphicon-ok"></span> สมัครสมาชิก  </button></center>
                </div>

            </div>
            </form>
            </center>
      </section>
    <!-- Footer -->
    <footer class="footer text-center">
      <div class="container">
        <div class="row">
          <div class="col-md-4 mb-5 mb-lg-0">
            <h4 class="text-uppercase mb-4">Location</h4>
            <p class="lead mb-0"> 2M airline @ Mahasarakham 1234</p>
          </div>
          <div class="col-md-4 mb-5 mb-lg-0">
            <h4 class="text-uppercase mb-4">Follow Me</h4>
            <ul class="list-inline mb-0">
              <li class="list-inline-item">
                <a class="btn btn-outline-light btn-social text-center rounded-circle" href="#">
                  <i class="fab fa-fw fa-facebook-f"></i>
                </a>
              </li>

               <li class="list-inline-item">
                <a class="btn btn-outline-light btn-social text-center rounded-circle" href="#">
                  <i class="fab fa-fw fa-facebook-f"></i>
                </a>
              </li>

            </ul>
          </div>
          <div class="col-md-4">
            <h4 class="text-uppercase mb-4">About</h4>
            <p class="lead mb-0">Project Term Web Programming 3CS 1/2561 @computer Science mahasarakham university thailand
          </div>
        </div>
      </div>
    </footer>


    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-to-top d-lg-none position-fixed ">
      <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top">
        <i class="fa fa-chevron-up"></i>
      </a>
    </div>



    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/freelancer.min.js"></script>


<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script language="javascript">
    $('.dropdown-toggle').dropdown();
    $('.dropdown-menu').find('form').click(function (e) {
        e.stopPropagation();
      });
</script>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
          td = tr[i].getElementsByTagName("td")[0];
          if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
              tr[i].style.display = "";
            } else {
              tr[i].style.display = "none";
            }
          }
        }
}
</script>

<script>
function myFunction1() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput1");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
          td = tr[i].getElementsByTagName("td")[1];
          if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
              tr[i].style.display = "";
            } else {
              tr[i].style.display = "none";
            }
          }
        }
}
</script>

<script>
function myFunction2() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput2");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
          td = tr[i].getElementsByTagName("td")[2];
          if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
              tr[i].style.display = "";
            } else {
              tr[i].style.display = "none";
            }
          }
        }
}
</script>

<script>
function myFunction3() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput3");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
          td = tr[i].getElementsByTagName("td")[4];
          if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
              tr[i].style.display = "";
            } else {
              tr[i].style.display = "none";
            }
          }
        }
}
</script>
  </body>

</html>
