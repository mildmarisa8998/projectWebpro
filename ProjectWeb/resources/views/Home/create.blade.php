<!DOCTYPE html>
<html lang="en">
<style>
      * {
        box-sizing: border-box;
      }

      #myInput,#myInput1,#myInput2,#myInput3 {
        background-image: url('/css/searchicon.png');
        background-position: 10px 10px;
        background-repeat: no-repeat;
        width: 15%;
        font-size: 16px;
        padding: 12px 20px 12px 40px;
        border: 1px solid #ddd;
        margin-bottom: 12px;
        margin-left: 10px;
      }

      #myTable {
        border-collapse: collapse;
        width: 100%;
        border: 1px solid #ddd;
        font-size: 18px;
        text-align: center;
      }

      #myTable th, #myTable td {
        text-align: left;
        padding: 12px;
        text-align: center;
      }

      #myTable tr {
        border-bottom: 1px solid #ddd;
      }

      #myTable tr.header, #myTable tr:hover {
        background-color: #f1f1f1;
      }
</style>
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>2M Airline</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- Plugin CSS -->
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="css/freelancer.min.css" rel="stylesheet">
    <link href="css/flexslider.css" rel="stylesheet">

    <link rel="stylesheet" href="/path/to/bootstrap.min.css">
    <script src="/path/to/jquery.min.js"></script>
    <script src="ccFileUpload.js"></script>
  </head>

  <body id="page-top">
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg bg-secondary fixed-top text-uppercase" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">2M Airline</a>
        <button class="navbar-toggler navbar-toggler-right text-uppercase bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#portfolio">เที่ยวบิน</a>
            </li>
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#about">ประวัติการจอง</a>
            </li>
          </ul>
        </div>
      <div class="dropdown" style="margin: 15px">
            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">welcome : <?=$_SESSION['account']?> <span class="caret"></span>
            </button>

            <ul class="dropdown-menu">
                  <!-- <li><a href="{{action('HomeController@edit',$data[0]->cid)}}">แก้ไขข้อมูลส่วนตัว</a></li> -->
                  <li><a href="{{action('HomeController@edit',$data[0]->cid)}}">แก้ไขข้อมูลส่วนตัว</a></li>
                  <!--go edit-->
                  <li><a href="{{url('Home')}}">ออกจากระบบ</a></li>
            </ul>
         </div>
      </div>
    </nav>

    <!-- Portfolio Grid Section -->
       <!-- Portfolio Grid Section -->
       <section class="portfolio" id="portfolio" style="margin-top:40px; ">
         <div class="container">
           <h2 class="text-center text-uppercase text-secondary mb-0">Flight</h2>
           <hr class="star-dark mb-5">
   <center>
           <input type="text" id="myInput" onkeyup="myFunction()"
             placeholder="ต้นทาง" title="Type in a name">

           <input type="text" id="myInput1" onkeyup="myFunction1()"
             placeholder="ปลายทาง" title="Type in a name">

             <input type="text" id="myInput2" onkeyup="myFunction2()"
             placeholder="วันออกเดินทาง" title="Type in a name">

             <input type="text" id="myInput3" onkeyup="myFunction3()"
             placeholder="วันกลับ" title="Type in a name">
   </center>
             <table id="myTable">
                 <tr class="header">
                   <th>Source</th>
                   <th>Destinetion</th>
                   <th>Depart</th>
                   <th>Return</th>
                   <th>Price</th>
                   <th>Class</th>
                   <th>Plane</th>
                   <th>จอง</th>
                 </tr>

                 @foreach($flight as $row)
                  <tr>
                 <form method="post" class="delete_form" action="/reserve/{{$row['fid']}}">
                   {{ csrf_field() }}

                   <td>{{$row['source']}}</td>
                   <td>{{$row['destinetion']}}</td>
                   <td>{{$row['startDate']}}</td>
                   <td>{{$row['endDate']}}</td>
                   <td>{{$row['price']}}</td>
                   <td>
                     <select name="value">
                        <option value="First class">First class</option>
                        <option value="Business Class ">Business Class</option>
                        <option value="Premium">Premium economy Class</option>
                        <option value="Economy">Economy Class</option>
                      </select>
                   </td>
                   <td>{{$row['planename']}}</td>
                    <td><button  type="submit" class="btn btn-danger">จอง</button></td>

               </form>

               </tr>
                 @endforeach
             </table>
             <center>
             <br><br>
             @if(count($errors)>0)
              <div class="aler alert-danger">
                <ul>@foreach($errors->all() as $error)
                  <li>{{$error}}</li>
                @endforeach
                </ul>
              </div>
            @endif

            @if(\Session::has('success'))
              <div class="alert alert-success">
                <p>{{\Session::get('success')}}</p>
                </div>
              @endif
            </center>
          </div>
     </section>

    <!-- About Section -->
    <section class="bg-primary text-white mb-0" id="about">
      <div class="container">
        <h2 class="text-center text-uppercase text-white">ประวัติการจอง</h2>
        <hr class="star-light mb-5">
          <div>
            <div class="row">
            <table class="table table-hover" style="background-color: #696969;">
              <thead>
              <tr>
                <th>Source</th>
                <th>Destinetion</th>
                <th>Depart</th>
                <th>Return</th>
                <th>Seate</th>
                <th>Price</th>
                <th>Plane</th>
                  <th></th>
                  <td></td>
              </tr>
              </thead>
              <tbody>

                  @foreach($his as $row)
                  <tr>
                    <td>{{$row->source}}</td>
                    <td>{{$row->destinetion}}</td>
                    <td>{{$row->startDate}}</td>
                    <td>{{$row->endDate}}</td>
                    <td>{{$row->seat}}</td>
                    <td>{{$row->price}}</td>
                    <td>{{$row->planename}}</td>
                    <td><button  type="submit" class="btn btn-danger">เช็คอิน</button></td>
                    <td> <form method="post" class="delete_form" action="{{action('HomeLoginController@destroy',$row->id)}}">
                          {{ csrf_field() }}
                          <input type="hidden" name="_method" value="DELETE"/>
                          <button  type="submit" class="btn btn-warning">Cancel</button>
                        </form>
                    </td>
									  <td>

                </tr>
                @endforeach

              </tbody>
          </table>
          <center><p style="font-size:20px;" >คำเตือน!! ปุ่มเช็คอิน กดเมื่อต้องการเช็คอิน เมื่อถึงสนามบินเเล้วเท่านั้น หากกดตอนยังไม่พร้อมที่จะขึ้นเครื่องอาจทำให้ท่าน ตกเครื่อง เเละค่าตั๋วเครื่องบินทางสายการบิน
          จะไม่ขอรับผิดชอบไม่ว่ากรณีใดๆทั้งสิ้น จึงเรียนมาเพื่อทราบ</p></center>
          </div>
        </div>
      </div>
          </div>

      </div>
    </section>

    <!-- Footer -->
    <footer class="footer text-center">
      <div class="container">
        <div class="row">
          <div class="col-md-4 mb-5 mb-lg-0">
            <h4 class="text-uppercase mb-4">Location</h4>
            <p class="lead mb-0"> 2M airline @ Mahasarakham 1234</p>
          </div>
          <div class="col-md-4 mb-5 mb-lg-0">
            <h4 class="text-uppercase mb-4">Follow Me</h4>
            <ul class="list-inline mb-0">
              <li class="list-inline-item">
                <a class="btn btn-outline-light btn-social text-center rounded-circle" href="#">
                  <i class="fab fa-fw fa-facebook-f"></i>
                </a>
              </li>

               <li class="list-inline-item">
                <a class="btn btn-outline-light btn-social text-center rounded-circle" href="#">
                  <i class="fab fa-fw fa-facebook-f"></i>
                </a>
              </li>

            </ul>
          </div>
          <div class="col-md-4">
            <h4 class="text-uppercase mb-4">About</h4>
            <p class="lead mb-0">Project Term Web Programming 3CS 1/2561 @computer Science mahasarakham university thailand
          </div>
        </div>
      </div>
    </footer>


    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-to-top d-lg-none position-fixed ">
      <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top">
        <i class="fa fa-chevron-up"></i>
      </a>
    </div>


    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/freelancer.min.js"></script>


<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script language="javascript">
    $('.dropdown-toggle').dropdown();
    $('.dropdown-menu').find('form').click(function (e) {
        e.stopPropagation();
      });
</script>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
          td = tr[i].getElementsByTagName("td")[0];
          if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
              tr[i].style.display = "";
            } else {
              tr[i].style.display = "none";
            }
          }
        }
}
</script>

<script>
function myFunction1() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput1");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
          td = tr[i].getElementsByTagName("td")[1];
          if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
              tr[i].style.display = "";
            } else {
              tr[i].style.display = "none";
            }
          }
        }
}
</script>

<script>
function myFunction2() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput2");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
          td = tr[i].getElementsByTagName("td")[2];
          if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
              tr[i].style.display = "";
            } else {
              tr[i].style.display = "none";
            }
          }
        }
}
</script>

<script>
function myFunction3() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput3");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
          td = tr[i].getElementsByTagName("td")[4];
          if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
              tr[i].style.display = "";
            } else {
              tr[i].style.display = "none";
            }
          }
        }
}
</script>

  </body>

</html>
