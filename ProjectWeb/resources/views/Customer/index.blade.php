@extends('master')
@section('title','Customer')
@section('head')
@stop
@section('content')
<div id="content-wrapper">

	<div class="container-fluid">
		<!-- @if(count($errors)>0)
			<div class="aler alert-danger">
				<ul>
				@foreach($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div>
		@endif

		@if(\Session::has('success'))
			<div class="alert alert-success">
			<p>{{\Session::get('success')}}</p>
			</div>
		@endif-->
		<!-- Breadcrumbs-->
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="#">Dashboard</a>
			</li>
			<li class="breadcrumb-item active">Customer</li>
		</ol>
		@if(count($errors)>0)
			<div class="aler alert-danger">
				<ul>
				@foreach($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div>
		@endif
		<div class="container mb-5">
			<a href="{{route('Customer.create')}}" class="btn btn-primary">Add Customer</a>
			<h1 class="display-4 mt-5 mb-5">Customer</h1>
			<div class="row">
				<div class="form-row w-100">
					<div class="table-responsive">
						<table class="table table-hover">
							<tr>
								<th>First Name</th>
								<th>Last Name</th>
								<th>Sex</th>
								<th>Email</th>
								<th>Phone</th>
								<th></th>
								<th></th>
							</tr>
							@foreach($customers as $row)
							<tr>
								<td>{{$row['firstname']}}</td>
								<td>{{$row['lastname']}}</td>
								<td>{{$row['sex']}}</td>
								<td>{{$row['email']}}</td>
								<td>{{$row['phone']}}</td>
								<td><a href="{{action('CustomerController@edit',$row['cid'])}}" class="btn btn-primary">Edite</td>
								<td>
									<form method="post" class="delete_form" action="{{action('CustomerController@destroy',$row['cid'])}}">
										{{ csrf_field() }}
									<input type="hidden" name="_method" value="DELETE"/>
									<button type="submit" class="btn btn-danger">Delete</button>
								</td>
							</tr>
							@endforeach
						</table>
					</tbody>
				</div>
			</div>
		</div>
	</div>
			<script type="text/javascript" src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
			<script type="text/javascript" src="{{ asset('bootstrap/js/bootstrap.bundle.js') }}"></script>

			<footer class="sticky-footer">
				<div class="container my-auto">
					<div class="copyright text-center my-auto">
						<span>Welcom to Airline</span>
					</div>
				</div>
			</footer>

		</div>
		@stop
		@section('footer')
		@stop
