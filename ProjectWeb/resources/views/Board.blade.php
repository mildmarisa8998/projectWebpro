@extends('master')
@section('title','Home')
@section('head')
@stop
@section('content')
<div id="content-wrapper">

  <div class="container-fluid">

    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="index.html">Dashboard</a>
      </li>
      <li class="breadcrumb-item active">Home</li>
    </ol>
  </div>

  <!-- /.container-fluid -->
  <div class="container-fluid">
        <!-- /.container-fluid --
          <!-- Sticky Footer -->
          <footer class="sticky-footer">
            <div class="container my-auto">
              <div class="copyright text-center my-auto">
                <span>Welcome to 2M Airline</span>
              </div>
            </div>
          </footer>

        </div>
        @stop
        @section('footer')
        @stop
