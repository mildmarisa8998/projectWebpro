<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>2M Airline</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- Plugin CSS -->
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="css/freelancer.min.css" rel="stylesheet">
    <link href="css/flexslider.css" rel="stylesheet">

    <link rel="stylesheet" href="/path/to/bootstrap.min.css">
    <script src="/path/to/jquery.min.js"></script>
    <script src="ccFileUpload.js"></script>



  </head>

  <body id="page-top">
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg bg-secondary fixed-top text-uppercase" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">customer</a>
        <button class="navbar-toggler navbar-toggler-right text-uppercase bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#portfolio">เที่ยวบิน</a>
            </li>
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#about">ประวัติการจอง</a>
            </li>
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#contact">แจ้งหลักฐานการชำระเงิน</a>
            </li>
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#about">เช็คอินที่สนามบิน</a>
            </li>
          </ul>
        </div>
      <div class="dropdown" style="margin: 15px">
            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">welcome :  <span class="caret"></span>
            </button>

            <ul class="dropdown-menu">
                  <li><a href="#">แก้ไขข้อมูลส่วนตัว</a></li>
                  <!--go edit-->
                  <li><a href={{url('index.php')}}>ออกจากระบบ</a></li>
            </ul>
         </div>
      </div>
    </nav>

    <!-- Portfolio Grid Section -->
    <section class="portfolio" id="portfolio" style="margin-top: 50px;">
      <div class="container">
        <h2 class="text-center text-uppercase text-secondary mb-0">เที่ยวบิน</h2>
        <hr class="star-dark mb-5">
        <div class="row">
          <div class="col-md-6 col-lg-4">
            <a class="portfolio-item d-block mx-auto" href="#portfolio-modal-1">
              <div class="portfolio-item-caption d-flex position-absolute h-100 w-100">
                <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
                  <i class="fas fa-search-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/portfolio/6.png" alt="">
            </a>
          </div>

          <div class="col-md-6 col-lg-4">
            <a class="portfolio-item d-block mx-auto" href="#portfolio-modal-2">
              <div class="portfolio-item-caption d-flex position-absolute h-100 w-100">
                <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
                  <i class="fas fa-search-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/portfolio/7.png" alt="">
            </a>
          </div>

          <div class="col-md-6 col-lg-4">
            <a class="portfolio-item d-block mx-auto" href="#portfolio-modal-3">
              <div class="portfolio-item-caption d-flex position-absolute h-100 w-100">
                <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
                  <i class="fas fa-search-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/portfolio/8.png" alt="">
            </a>
          </div>

          <div class="col-md-6 col-lg-4">
            <a class="portfolio-item d-block mx-auto" href="#portfolio-modal-4">
              <div class="portfolio-item-caption d-flex position-absolute h-100 w-100">
                <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
                  <i class="fas fa-search-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/portfolio/9.png" alt="">
            </a>
          </div>

          <div class="col-md-6 col-lg-4">
            <a class="portfolio-item d-block mx-auto" href="#portfolio-modal-5">
              <div class="portfolio-item-caption d-flex position-absolute h-100 w-100">
                <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
                  <i class="fas fa-search-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/portfolio/10.png" alt="">
            </a>
          </div>

          <div class="col-md-6 col-lg-4">
            <a class="portfolio-item d-block mx-auto" href="#portfolio-modal-6">
              <div class="portfolio-item-caption d-flex position-absolute h-100 w-100">
                <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
                  <i class="fas fa-search-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/portfolio/11.png" alt="">
            </a>
          </div>
        </div>

      </div>
    </section>

    <!-- About Section -->
    <section class="bg-primary text-white mb-0" id="about">
      <div class="container">
        <h2 class="text-center text-uppercase text-white">ประวัติการจอง</h2>
        <hr class="star-light mb-5">
          <div>
            <div class="row">
            <table class="table table-hover" style="background-color: Black;">
              <thead>
              <tr>
                  <th>ต้นทาง</th>
                  <th>ปลายทาง</th>
                  <th>start time</th>
                  <th>end time</th>
                  <th>ราคา (บาท)</th>
                  <th>สถานะการชำระเงิน</th>
                   <th>เวลาที่อัพเดต</th>
                  <th>วันที่อัพเดต</th>
                  <th>ยกเลิกการจอง</th>
                  <td>เช็คอินที่สนามบิน</td>
              </tr>
              </thead>
              <tbody>
                <tr>
                    <td>bankok</td>
                    <td>zurich</td>
                    <td>08.00</td>
                    <td>23.30</td>
                    <td>233</td>
                    <td>  <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="#">
                  <i class="fa fa-close"></i>
                  ชำระเเล้ว</a></td>
                   <td>13.00</td>
                   <td>20/11/12</td>
                  <td><a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" style="background-color: red;" href="#">
                   ยกเลิกการจอง</a></td>
                   <td>  <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="#">
                   <i class="fa fa-close"></i>
                   เช็คอิน</a></td>
                 </tr>

                 <tr>
                    <td>bankok</td>
                    <td>zurich</td>
                    <td>08.00</td>
                    <td>23.30</td>
                    <td>233</td>
                    <td>  <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="#">
                  <i class="fa fa-close"></i>
                  ชำระเเล้ว</a></td>
                  <td>13.00</td>
                   <td>20/11/12</td>
                  <td><a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" style="background-color: red;" href="#">
                   ยกเลิกการจอง</a></td>
                   <td>  <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="#">
                   <i class="fa fa-close"></i>
                   เช็คอิน</a></td>
                </tr>
                 <tr>
                    <td>bankok</td>
                    <td>zurich</td>
                    <td>08.00</td>
                    <td>23.30</td>
                    <td>233</td>
                    <td>  <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="#">
                  <i class="fa fa-close"></i>
                  ยังไม่ชำระ</a></td>
                   <td> - </td>
                    <td> - </td>
                   <td><a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" style="background-color: red;" href="#">
                   ยกเลิกการจอง</a></td>
                   <td>  <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="#">

                   เช็คอิน</a></td>
                </tr>
                 <tr>
                    <td>bankok</td>
                    <td>zurich</td>
                    <td>08.00</td>
                    <td>23.30</td>
                    <td>233</td>
                    <td>  <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="#">
                  <i class="fa fa-close"></i>
                  ยังไม่ชำระ</a></td>
                   <td> - </td>
                    <td> - </td>
                   <td><a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" style="background-color: red;" href="#">
                   ยกเลิกการจอง</a></td>
                   <td>  <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="#">
                   <i class="fa fa-close"></i>
                   เช็คอิน</a></td>
                </tr>
              </tbody>
          </table>
          <center><p style="font-size:20px;" >คำเตือน!! ปุ่มเช็คอิน กดเมื่อต้องการเช็คอิน เมื่อถึงสนามบินเเล้วเท่านั้น หากกดตอนยังไม่พร้อมที่จะขึ้นเครื่องอาจทำให้ท่าน ตกเครื่อง เเละค่าตั๋วเครื่องบินทางสายการบิน
          จะไม่ขอรับผิดชอบไม่ว่ากรณีใดๆทั้งสิ้น จึงเรียนมาเพื่อทราบ</p></center>
          </div>
        </div>
      </div>
          </div>

      </div>
    </section>

    <!-- Contact Section -->
    <section id="contact">
      <div class="container">
        <h2 class="text-center text-uppercase text-secondary mb-0">หลักฐานการชำระเงิน</h2>
        <hr class="star-dark mb-5">
        <div class="row">
          <div class="col-lg-8 mx-auto">
            <form method="post" enctype="multipart/form-data" action="">
                  <div class="col-xs-12">
                      <div id="demo" class="text-center"></div>
                  </div>
                  <div class="col-xs-12">
                      <span class="label label-info pull-right"><span class="countMe">0</span> files ready to upload.</span>
                      <hr>
                      <div id="stage" style="max-height:400px; overflow-y:scroll"></div>
                      <hr>
                  </div>
                  <div class="col-xs-12 text-center">
                      <input type="submit" value="Upload" class="btn btn-primary">
                  </div>
            </form>

          </div>
        </div>
      </div>
    </section>


    <!-- Footer -->
    <footer class="footer text-center">
      <div class="container">
        <div class="row">
          <div class="col-md-4 mb-5 mb-lg-0">
            <h4 class="text-uppercase mb-4">Location</h4>
            <p class="lead mb-0"> 2M airline @ Mahasarakham 1234</p>
          </div>
          <div class="col-md-4 mb-5 mb-lg-0">
            <h4 class="text-uppercase mb-4">Follow Me</h4>
            <ul class="list-inline mb-0">
              <li class="list-inline-item">
                <a class="btn btn-outline-light btn-social text-center rounded-circle" href="#">
                  <i class="fab fa-fw fa-facebook-f"></i>
                </a>
              </li>

               <li class="list-inline-item">
                <a class="btn btn-outline-light btn-social text-center rounded-circle" href="#">
                  <i class="fab fa-fw fa-facebook-f"></i>
                </a>
              </li>

            </ul>
          </div>
          <div class="col-md-4">
            <h4 class="text-uppercase mb-4">About</h4>
            <p class="lead mb-0">Project Term Web Programming 3CS 1/2561 @computer Science mahasarakham university thailand
          </div>
        </div>
      </div>
    </footer>


    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-to-top d-lg-none position-fixed ">
      <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top">
        <i class="fa fa-chevron-up"></i>
      </a>
    </div>

    <!-- Portfolio Modals -->

    <!-- Portfolio Modal 1 -->
    <div class="portfolio-modal mfp-hide" id="portfolio-modal-1">
      <div class="portfolio-modal-dialog bg-white">
        <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="#">
          <i class="fa fa-3x fa-times"></i>
        </a>
        <div class="container text-center">
          <div class="row">
            <table class="table table-hover">
              <thead>
              <tr>
                  <th>ต้นทาง</th>
                  <th>ปลายทาง</th>
                  <th>start time</th>
                  <th>end time</th>
                  <th>ราคา (บาท)</th>
                  <th>จองที่นั่ง</th>
              </tr>
              </thead>
              <tbody>
                <tr>
                    <td>bankok</td>
                    <td>zurich</td>
                    <td>08.00</td>
                    <td>23.30</td>
                    <td>233</td>
                    <td> <a href= "\seatAirline">จอง</a></td>
                </tr>
                 <tr>
                    <td>bankok</td>
                    <td>zurich</td>
                    <td>08.00</td>
                    <td>23.30</td>
                    <td>233</td>
                    <td> <a href= "\seatAirline">จอง</a></td>
                </tr>
                 <tr>
                    <td>bankok</td>
                    <td>zurich</td>
                    <td>08.00</td>
                    <td>23.30</td>
                    <td>233</td>
                    <td> <a href= "\seatAirline">จอง</a></td>
                </tr>
                 <tr>
                    <td>bankok</td>
                    <td>zurich</td>
                    <td>08.00</td>
                    <td>23.30</td>
                    <td>233</td>
                  <td> <a href= "\seatAirline">จอง</a></td>
                </tr>
              </tbody>
          </table>
          </div>
        </div>
      </div>
    </div>

    <!-- Portfolio Modal 2 -->
    <div class="portfolio-modal mfp-hide" id="portfolio-modal-2">
      <div class="portfolio-modal-dialog bg-white">
        <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="#">
          <i class="fa fa-3x fa-times"></i>
        </a>
        <div class="container text-center">
          <div class="row">
            <table class="table table-hover">
              <thead>
              <tr>
                  <th>ต้นทาง</th>
                  <th>ปลายทาง</th>
                  <th>start time</th>
                  <th>end time</th>
                  <th>ราคา (บาท)</th>
                  <th>จองที่นั่ง</th>
              </tr>
              </thead>
              <tbody>
                <tr>
                    <td>bankok</td>
                    <td>Sydney</td>
                    <td>08.00</td>
                    <td>23.30</td>
                    <td>233</td>
                  <td> <a href= "\seatAirline">จอง</a></td>
                </tr>
                 <tr>
                    <td>bankok</td>
                    <td>Sydney</td>
                    <td>08.00</td>
                    <td>23.30</td>
                    <td>233</td>
                  <td> <a href= "\seatAirline">จอง</a></td>
                </tr>
                 <tr>
                    <td>bankok</td>
                    <td>Sydney</td>
                    <td>08.00</td>
                    <td>23.30</td>
                    <td>233</td>
                     <td> <a href= "\seatAirline">จอง</a></td>
                </tr>
                 <tr>
                    <td>bankok</td>
                    <td>Sydney</td>
                    <td>08.00</td>
                    <td>23.30</td>
                    <td>233</td>
                     <td> <a href= "\seatAirline">จอง</a></td>
                </tr>
              </tbody>
          </table>
          </div>
        </div>
      </div>
    </div>

    <!-- Portfolio Modal 3 -->
    <div class="portfolio-modal mfp-hide" id="portfolio-modal-3">
      <div class="portfolio-modal-dialog bg-white">
        <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="#">
          <i class="fa fa-3x fa-times"></i>
        </a>
        <div class="container text-center">
          <div class="row">
            <table class="table table-hover">
              <thead>
              <tr>
                  <th>ต้นทาง</th>
                  <th>ปลายทาง</th>
                  <th>start time</th>
                  <th>end time</th>
                  <th>ราคา (บาท)</th>
                  <th>จองที่นั่ง</th>
              </tr>
              </thead>
              <tbody>
                <tr>
                    <td>bankok</td>
                    <td>maldive</td>
                    <td>08.00</td>
                    <td>23.30</td>
                    <td>233</td>
                     <td> <a href= "\seatAirline">จอง</a></td>
                </tr>
                 <tr>
                    <td>bankok</td>
                    <td>maldive</td>
                    <td>08.00</td>
                    <td>23.30</td>
                    <td>233</td>
                  <td> <a href= "\seatAirline">จอง</a></td>
                </tr>
                 <tr>
                    <td>bankok</td>
                    <td>maldive</td>
                    <td>08.00</td>
                    <td>23.30</td>
                    <td>233</td>
                 <td> <a href= "\seatAirline">จอง</a></td>
                </tr>
                 <tr>
                    <td>bankok</td>
                    <td>maldive</td>
                    <td>08.00</td>
                    <td>23.30</td>
                    <td>233</td>
                  <td> <a href= "\seatAirline">จอง</a></td>
                </tr>
              </tbody>
          </table>
          </div>
        </div>
      </div>
    </div>

    <!-- Portfolio Modal 4 -->
    <div class="portfolio-modal mfp-hide" id="portfolio-modal-4">
      <div class="portfolio-modal-dialog bg-white">
        <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="#">
          <i class="fa fa-3x fa-times"></i>
        </a>
        <div class="container text-center">
          <div class="row">
            <table class="table table-hover">
              <thead>
              <tr>
                  <th>ต้นทาง</th>
                  <th>ปลายทาง</th>
                  <th>start time</th>
                  <th>end time</th>
                  <th>ราคา (บาท)</th>
                  <th>จองที่นั่ง</th>
              </tr>
              </thead>
              <tbody>
                <tr>
                    <td>bankok</td>
                    <td>Los</td>
                    <td>08.00</td>
                    <td>23.30</td>
                    <td>233</td>
                   <td> <a href= "\seatAirline">จอง</a></td>
                </tr>
                 <tr>
                    <td>bankok</td>
                    <td>Los</td>
                    <td>08.00</td>
                    <td>23.30</td>
                    <td>233</td>
                    <td> <a href= "\seatAirline">จอง</a></td>
                </tr>
                 <tr>
                    <td>bankok</td>
                    <td>Los</td>
                    <td>08.00</td>
                    <td>23.30</td>
                    <td>233</td>
                   <td> <a href= "\seatAirline">จอง</a></td>
                </tr>
                 <tr>
                    <td>bankok</td>
                    <td>Los</td>
                    <td>08.00</td>
                    <td>23.30</td>
                    <td>233</td>
                   <td> <a href= "\seatAirline">จอง</a></td>
                </tr>
              </tbody>
          </table>
          </div>
        </div>
      </div>
    </div>

    <!-- Portfolio Modal 5 -->
    <div class="portfolio-modal mfp-hide" id="portfolio-modal-5">
      <div class="portfolio-modal-dialog bg-white">
        <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="#">
          <i class="fa fa-3x fa-times"></i>
        </a>
        <div class="container text-center">
          <div class="row">
           <table class="table table-hover">
              <thead>
              <tr>
                  <th>ต้นทาง</th>
                  <th>ปลายทาง</th>
                  <th>start time</th>
                  <th>end time</th>
                  <th>ราคา (บาท)</th>
                  <th>จองที่นั่ง</th>
              </tr>
              </thead>
              <tbody>
                <tr>
                    <td>bankok</td>
                    <td>London</td>
                    <td>08.00</td>
                    <td>23.30</td>
                    <td>233</td>
                   <td> <a href= "\seatAirline">จอง</a></td>
                </tr>
                 <tr>
                    <td>bankok</td>
                    <td>London</td>
                    <td>08.00</td>
                    <td>23.30</td>
                    <td>233</td>
                   <td> <a href= "\seatAirline">จอง</a></td>l portfolio-modal-dismiss"
                      href= "\seatAirline">จอง</a></td>
                </tr>
                 <tr>
                    <td>bankok</td>
                    <td>London</td>
                    <td>08.00</td>
                    <td>23.30</td>
                    <td>233</td>
                   <td> <a href= "\seatAirline">จอง</a></td>
                </tr>
                 <tr>
                    <td>bankok</td>
                    <td>London</td>
                    <td>08.00</td>
                    <td>23.30</td>
                    <td>233</td>
                   <td> <a href= "\seatAirline">จอง</a></td>
                </tr>
              </tbody>
          </table>
          </div>
        </div>
      </div>
    </div>

    <!-- Portfolio Modal 6 -->
    <div class="portfolio-modal mfp-hide" id="portfolio-modal-6">
      <div class="portfolio-modal-dialog bg-white">
        <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="#">
          <i class="fa fa-3x fa-times"></i>
        </a>
        <div class="container text-center">
          <div class="row">
           <table class="table table-hover">
              <thead>
              <tr>
                  <th>ต้นทาง</th>
                  <th>ปลายทาง</th>
                  <th>start time</th>
                  <th>end time</th>
                  <th>ราคา (บาท)</th>
                  <th>จองที่นั่ง</th>
              </tr>
              </thead>
              <tbody>
                <tr>
                    <td>bankok</td>
                    <td>London</td>
                    <td>08.00</td>
                    <td>23.30</td>
                    <td>233</td>
                  <td> <a href= "\seatAirline">จอง</a></td>
                </tr>
                 <tr>
                    <td>bankok</td>
                    <td>London</td>
                    <td>08.00</td>
                    <td>23.30</td>
                    <td>233</td>
                 <td> <a href= "\seatAirline">จอง</a></td>
                </tr>
                 <tr>
                    <td>bankok</td>
                    <td>London</td>
                    <td>08.00</td>
                    <td>23.30</td>
                    <td>233</td>
                  <td> <a href= "\seatAirline">จอง</a></td>
                </tr>
                 <tr>
                    <td>bankok</td>
                    <td>London</td>
                    <td>08.00</td>
                    <td>23.30</td>
                    <td>233</td>
               <td> <a href= "\seatAirline">จอง</a></td>
                </tr>
              </tbody>
          </table>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/freelancer.min.js"></script>


<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script language="javascript">
    $('.dropdown-toggle').dropdown();
    $('.dropdown-menu').find('form').click(function (e) {
        e.stopPropagation();
      });
</script>
  </body>

</html>
