@extends('master')
@section('title','Flight')
@section('head')
@stop
@section('content')
<div id="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="#">Dashboard</a>
			</li>
			<li class="breadcrumb-item active">Flight</li>
		</ol>
		<!-- Icon Cards-->
		<div id="content-wrapper">
			<div class="container mb-5">
				<a href="{{route('Flight.create')}}">
					<button type="submit" class="btn btn-primary">Add Flight</button>
				</a>
				<h1 class="display-4 mt-5 mb-5">Flight</h1>
				<div class="row">
					<div class="form-row w-100">
						<div class="table-responsive">
							<table class="table table-hover">
								<tr>
									<th>Plane</th>
									<th>Source</th>
									<th>Destinetion</th>
									<th>Depart</th>
									<th>Return</th>
									<th>Price</th>
									<th></th>
									<th></th>
								</tr>
								@foreach($flight as $row)
								<tr>
									<td>{{$row['planename']}}</td>
									<td>{{$row['source']}}</td>
									<td>{{$row['destinetion']}}</td>
									<td>{{$row['startDate']}}</td>
									<td>{{$row['endDate']}}</td>
									<td>{{$row['price']}}</td>
									<td><a href="{{action('FlightController@edit',$row['fid'])}}" class="btn btn-primary">Edite</td>
									<td>
										<form method="post" class="delete_form" action="{{action('FlightController@destroy',$row['fid'])}}">
											{{ csrf_field() }}
										<input type="hidden" name="_method" value="DELETE"/>
										<button type="submit" class="btn btn-danger">Delete</button>
									</form>
									</td>
								</tr>
								@endforeach
							</table>
						</tbody>
					</div>
				</div>
			</div>
		</div>
				<script type="text/javascript" src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
				<script type="text/javascript" src="{{ asset('bootstrap/js/bootstrap.bundle.js') }}"></script>

				<!-- Sticky Footer -->
				<footer class="sticky-footer">
					<div class="container my-auto">
						<div class="copyright text-center my-auto">
							<span>Welcom to Airline</span>
						</div>
					</div>
				</footer>

			</div>
			<!-- /.container-fluid -->

			<!-- Sticky Footer -->
			<footer class="sticky-footer">
				<div class="container my-auto">
					<div class="copyright text-center my-auto">
						<span>Welcom to Airline</span>
					</div>
				</div>
			</footer>

		</div>
		@stop
		@section('footer')
		@stop
