@extends("Board")
@section("title","Edite Myself Admin")
@section('head')
@stop
@section("content")
<div class="container mt-5 mb-5">
	<h1 class="display-4 mt-5 mb-5">Edite Admin My Profile</h1>
	<div class="row">
		<div class="col-lg-4">
			<form action="addminsUpdate" method="post" class="pt-5 pb-5">
				{{ csrf_field() }}
			
				<div class="form-group">
					<label for="user">ID</label>
					<input type="text" class="form-control" id="user" placeholder="user" name="user"
					 value="<?=$admins[0]->user?>">
				</div>
				<div class="form-group">
					<label for="fname">Name</label>
					<input type="text" class="form-control" id="fname" placeholder="First Name" name="fname" 
					value="<?=$admins[0]->name?>">
				</div>
				<div class="form-group">
					<label for="password">Password</label>
					<input type="text" class="form-control" id="password" placeholder="Password" name="password"
					value="<?=$admins[0]->password?>">
				</div>
				<div class="form-group">
					<input type="hidden" class="form-control" id="id" placeholder="user" name="id"
					 value="<?=$admins[0]->Aid?>">
				</div>
				<div class="form-group">
					<!-- <input type="hidden" name="_method" value="PATCH"/> -->
					<a href=""><button type="submit" class="btn btn-primary"> Update </button></a>
				</div>
			</form>
		</div>
	</div>
</div>
@stop
@section("footer")
@stop
