<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>2M Airline</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- Plugin CSS -->
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="css/freelancer.min.css" rel="stylesheet">
    <link href="css/flexslider.css" rel="stylesheet">
    
    <link rel="stylesheet" href="/path/to/bootstrap.min.css">
    <script src="/path/to/jquery.min.js"></script>
    <script src="ccFileUpload.js"></script>

    <link rel="stylesheet" type="text/css" href="css/jquery.seat-charts.css">
    <link rel="stylesheet" type="text/css" href="css/seat.css">
  </head>

  <body id="page-top">
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg bg-secondary fixed-top text-uppercase" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">Book air ticket</a>

          <div class="dropdown" style="margin: 15px">
            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">welcome Marisa <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                  <li><a href="\edit">แก้ไขข้อมูลส่วนตัว</a></li>
                  <li><a href="#">ออกจากระบบ</a></li>
            </ul>
         </div>
      </div>
    </nav> 
    <br><br>

   <section id="create">
        <h2 class="text-center text-uppercase text-secondary mb-0">Select seat</h2>
        <hr class="star-dark mb-5">
   
      <div class="container1" style="margin-left: 30%" >
           <div id="seat-map"></div>
      </div>
            <hr class="star-dark mb-5">

          <div class="booking-details" style=" margin-left: 30%">
            <h1> Selected Seats (<span id="counter">0</span>):</h1>

            <ul id="selected-seats" style=" margin-left: 10%">
            </ul>

            Total: 
            <b>$<span id="total">0</span></b>

            <a href="\confrirmSeat">
            <button class="checkout-button" style=" margin-left: 45%">confrirmSeat &raquo;</button>
          </div>
    

    </section>
    
    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-to-top d-lg-none position-fixed ">
      <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top">
        <i class="fa fa-chevron-up"></i>
      </a>
    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/freelancer.min.js"></script>


<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script language="javascript">
    $('.dropdown-toggle').dropdown();
    $('.dropdown-menu').find('form').click(function (e) {
        e.stopPropagation();
      });
</script>

<script src="http://code.jquery.com/jquery-1.12.4.min.js"></script> 
<script src="/js/jquery.seat-charts.js"></script> 

<script>
      var firstSeatLabel = 1;
    
      $(document).ready(function() {
        var $cart = $('#selected-seats'),
          $counter = $('#counter'),
          $total = $('#total'),
          sc = $('#seat-map').seatCharts({
          map: [
            '_ffff_ffff_ffff_ffff',
            '_ffff_ffff_ffff_ffff',
            '____________________',
            '_eeee_eeee_eeee_eeee',
            '_eeee_eeee_eeee_eeee',
            '____________________',
            '_eeee_eeee_eeee_eeee',
            '_eeee_eeee_eeee_eeee',
          ],
          seats: {
            f: {
              price   : 2000,
              classes : 'first-class', //your custom CSS class
              category: 'First Class'
            },
            e: {
              price   : 1000,
              classes : 'economy-class', //your custom CSS class
              category: 'Economy Class'
            } 
          },
          naming : {
            top : false,
            getLabel : function (character, row, column) {
              return firstSeatLabel++;
            },
          },
          legend : {
            node : $('#legend'),
              items : [
              [ 'f', 'available',   'First Class' ],
              [ 'e', 'available',   'Economy Class'],
              [ 'f', 'unavailable', 'Already Booked']
              ]         
          },
          click: function () {
            if (this.status() == 'available') {
              //let's create a new <li> which we'll add to the cart items
              $('<li>'+this.data().category+' Seat # '+this.settings.label+': <b>$'+this.data().price+'</b> <a href="#" class="cancel-cart-item">[cancel]</a></li>')
                .attr('id', 'cart-item-'+this.settings.id)
                .data('seatId', this.settings.id)
                .appendTo($cart);
              
              /*
               * Lets update the counter and total
               *
               * .find function will not find the current seat, because it will change its stauts only after return
               * 'selected'. This is why we have to add 1 to the length and the current seat price to the total.
               */
              $counter.text(sc.find('selected').length+1);
              $total.text(recalculateTotal(sc)+this.data().price);
              
              return 'selected';
            } else if (this.status() == 'selected') {
              //update the counter
              $counter.text(sc.find('selected').length-1);
              //and total
              $total.text(recalculateTotal(sc)-this.data().price);
            
              //remove the item from our cart
              $('#cart-item-'+this.settings.id).remove();
            
              //seat has been vacated
              return 'available';
            } else if (this.status() == 'unavailable') {
              //seat has been already booked
              return 'unavailable';
            } else {
              return this.style();
            }
          }
        });

        //this will handle "[cancel]" link clicks
        $('#selected-seats').on('click', '.cancel-cart-item', function () {
          //let's just trigger Click event on the appropriate seat, so we don't have to repeat the logic here
          sc.get($(this).parents('li:first').data('seatId')).click();
        });

        //let's pretend some seats have already been booked ************
        //sc.get(['1_2', '4_1', '7_1', '7_2']).status('unavailable');
    
    });

    function recalculateTotal(sc) {
      var total = 0;
    
      //basically find every selected seat and sum its price
      sc.find('selected').each(function () {
        total += this.data().price;
      });
      
      return total;
    }
    </script>


  </body>

</html>
